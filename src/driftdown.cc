/*****************************************************************************/

/*
 *      driftdown.cc  --  Drift Down Calculations.
 *
 *      Copyright (C) 2014, 2015, 2016, 2017, 2019  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include "driftdown.h"
#include "aircraft.h"
#include "baro.h"
#include "wind.h"

#include <iomanip>
#include <fstream>

#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <unsupported/Eigen/Polynomials>
#endif

DriftDown::QueuePt::QueuePt(int lat, int lon, int32_t alt)
	: m_lat(lat), m_lon(lon), m_alt(alt)
{
}

int DriftDown::QueuePt::compare(const QueuePt& x) const
{
	if (get_alt() < x.get_alt())
		return -1;
	if (x.get_alt() < get_alt())
		return 1;
	if (get_lat() < x.get_lat())
		return -1;
	if (x.get_lat() < get_lat())
		return 1;
	if (get_lon() < x.get_lon())
		return -1;
	if (x.get_lon() < get_lon())
		return 1;
	return 0;
}

int DriftDown::Edge::compare(const Edge& x) const
{
	if (get_startedge() < x.get_startedge())
		return -1;
	if (x.get_startedge() < get_startedge())
		return 1;
	if (get_endedge() < x.get_endedge())
		return -1;
	if (x.get_endedge() < get_endedge())
		return 1;
	return 0;
}

DriftDown::Neighbor::Neighbor(int latdisp, int londisp)
	: m_latdisp(latdisp), m_londisp(londisp), m_maxalt(std::numeric_limits<int32_t>::min())
{
}

int32_t DriftDown::Neighbor::get_alt(int32_t alt1) const
{
	if (alt1 > m_maxalt)
		return std::numeric_limits<int32_t>::max();
	return Point::round<int32_t,double>(m_altpoly.eval(alt1));
}

void DriftDown::Neighbor::set_altpoly(const poly_t& ap, int32_t ma)
{
	m_altpoly = ap;
	m_maxalt = ma;
}

const unsigned int DriftDown::nrweather;
const DriftDown::GridPt DriftDown::dummydata(std::numeric_limits<int32_t>::max(),
					     std::numeric_limits<int32_t>::max());

DriftDown::DriftDown(const Point& pt, int32_t elev)
	: m_dummydata(std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()),
	  m_minefftime(std::numeric_limits<gint64>::max()), m_maxefftime(std::numeric_limits<gint64>::min()),
	  m_minreftime(std::numeric_limits<gint64>::max()), m_maxreftime(std::numeric_limits<gint64>::min()),
	  m_coord(pt), m_elev(elev), m_latsize(0), m_lonsize(0), m_qnh(std::numeric_limits<float>::quiet_NaN())
{
}

void DriftDown::load_topo(TopoDb30& db, double maxradius)
{
	m_data.clear();
	m_latsize = m_lonsize = 0;
	if (m_coord.is_invalid())
		return;
	Rect bbox(m_coord.simple_box_nmi(maxradius));
        TopoDb30::TopoCoordinate tmin1(bbox.get_southwest()), tmax1(bbox.get_northeast());
        m_topoorigin = tmin1;
	m_latsize = tmin1.lat_dist(tmax1) + 1;
	m_lonsize = tmin1.lon_dist(tmax1) + 1;
	m_data.clear();
	m_data.resize(m_latsize * m_lonsize, GridPt(std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max()));
	for (TopoDb30::TopoTileCoordinate tmin(tmin1), tmax(tmax1), tc(tmin);;) {
                TopoDb30::pixel_index_t ymin((tc.get_lat_tile() == tmin.get_lat_tile()) ? tmin.get_lat_offs() : 0);
                TopoDb30::pixel_index_t ymax((tc.get_lat_tile() == tmax.get_lat_tile()) ? tmax.get_lat_offs() : TopoDb30::tile_size - 1);
                TopoDb30::pixel_index_t xmin((tc.get_lon_tile() == tmin.get_lon_tile()) ? tmin.get_lon_offs() : 0);
                TopoDb30::pixel_index_t xmax((tc.get_lon_tile() == tmax.get_lon_tile()) ? tmax.get_lon_offs() : TopoDb30::tile_size - 1);
                for (TopoDb30::pixel_index_t y(ymin); y <= ymax; ++y) {
			tc.set_lat_offs(y);
			for (TopoDb30::pixel_index_t x(xmin); x <= xmax; x++) {
				tc.set_lon_offs(x);
				TopoDb30::elev_t e(db.get_elev(tc));
				if (e == TopoDb30::nodata || e == TopoDb30::ocean)
					e = 0;
				operator()(tc).set_elev(Point::round<int32_t,float>(e * Point::m_to_ft));
			}
		}
		if (tc.get_lon_tile() != tmax.get_lon_tile()) {
			tc.advance_tile_east();
                        continue;
                }
                if (tc.get_lat_tile() == tmax.get_lat_tile())
                        break;
                tc.set_lon_tile(tmin.get_lon_tile());
                tc.advance_tile_north();
        }
}

void DriftDown::load_wind(const GRIB2& wxdb, gint64 efftime)
{
	m_minefftime = std::numeric_limits<gint64>::max();
	m_maxefftime = std::numeric_limits<gint64>::min();
	m_minreftime = std::numeric_limits<gint64>::max();
	m_maxreftime = std::numeric_limits<gint64>::min();
	m_qnh = std::numeric_limits<float>::quiet_NaN();
	for (unsigned int i = 0; i < nrweather; ++i) {
		m_wind[i] = gribwind_t(std::numeric_limits<float>::quiet_NaN(), std::numeric_limits<float>::quiet_NaN());
		m_temp[i] = std::numeric_limits<float>::quiet_NaN();
	}
	if (!&wxdb || m_coord.is_invalid()) {
		return;
	}
	Rect bbox(m_coord.simple_box_nmi(50));
	{
		GRIB2::layerlist_t ll(const_cast<GRIB2&>(wxdb).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_mass_prmsl), efftime, GRIB2::Layer::statproc_t::none));
		boost::intrusive_ptr<GRIB2::LayerInterpolateResult> wxprmsl(GRIB2::interpolate_results(bbox, ll, efftime));
   		if (wxprmsl) {
			m_minefftime = std::min(m_minefftime, wxprmsl->get_minefftime());
			m_maxefftime = std::max(m_maxefftime, wxprmsl->get_maxefftime());
			m_minreftime = std::min(m_minreftime, wxprmsl->get_minreftime());
			m_maxreftime = std::max(m_maxreftime, wxprmsl->get_maxreftime());
			m_qnh = wxprmsl->operator()(m_coord, efftime, 0) * 0.01;
		}
	}
	for (unsigned int i = 0; i < nrweather; ++i) {
		double pressure(100.0 * GRIB2::WeatherProfilePoint::isobaric_levels[i]);
		bool gndchart(pressure <= 0);
		{
			GRIB2::layerlist_t llu(const_cast<GRIB2&>(wxdb).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_momentum_ugrd), efftime, GRIB2::Layer::statproc_t::none,
										    gndchart ? GRIB2::surface_specific_height_gnd : GRIB2::surface_isobaric_surface,
										    gndchart ? 100 : pressure));
			boost::intrusive_ptr<GRIB2::LayerInterpolateResult> wxwindu(GRIB2::interpolate_results(bbox, llu, efftime, 100));
			GRIB2::layerlist_t llv(const_cast<GRIB2&>(wxdb).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_momentum_vgrd), efftime, GRIB2::Layer::statproc_t::none,
										    gndchart ? GRIB2::surface_specific_height_gnd : GRIB2::surface_isobaric_surface,
										    gndchart ? 100 : pressure));
			boost::intrusive_ptr<GRIB2::LayerInterpolateResult> wxwindv(GRIB2::interpolate_results(bbox, llv, efftime, 100));
			if (wxwindu && wxwindv) {
				m_minefftime = std::min(m_minefftime, wxwindu->get_minefftime());
				m_maxefftime = std::max(m_maxefftime, wxwindu->get_maxefftime());
				m_minreftime = std::min(m_minreftime, wxwindu->get_minreftime());
				m_maxreftime = std::max(m_maxreftime, wxwindu->get_maxreftime());
				m_minefftime = std::min(m_minefftime, wxwindv->get_minefftime());
				m_maxefftime = std::max(m_maxefftime, wxwindv->get_maxefftime());
				m_minreftime = std::min(m_minreftime, wxwindv->get_minreftime());
				m_maxreftime = std::max(m_maxreftime, wxwindv->get_maxreftime());
				m_wind[i] = wxwindu->get_layer()->get_grid()->transform_axes(wxwindu->operator()(m_coord, efftime, gndchart ? 100 : pressure),
											     wxwindv->operator()(m_coord, efftime, gndchart ? 100 : pressure));
			}
		}
		{
			GRIB2::layerlist_t ll(const_cast<GRIB2&>(wxdb).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_temperature_tmp), efftime, GRIB2::Layer::statproc_t::none,
										   gndchart ? GRIB2::surface_ground_or_water : GRIB2::surface_isobaric_surface,
										   gndchart ? 0 : pressure));
			boost::intrusive_ptr<GRIB2::LayerInterpolateResult> wxtemperature(GRIB2::interpolate_results(bbox, ll, efftime));
			if (wxtemperature) {
				m_minefftime = std::min(m_minefftime, wxtemperature->get_minefftime());
				m_maxefftime = std::max(m_maxefftime, wxtemperature->get_maxefftime());
				m_minreftime = std::min(m_minreftime, wxtemperature->get_minreftime());
				m_maxreftime = std::max(m_maxreftime, wxtemperature->get_maxreftime());
				m_temp[i] = wxtemperature->operator()(m_coord, efftime, gndchart ? 0 : pressure);
			}
		}
	}
}

DriftDown::GridPt& DriftDown::operator()(int lat, int lon)
{
	if (lat < 0 || lon < 0 || lat >= m_latsize || lon >= m_lonsize) {
		m_dummydata.set_elev(std::numeric_limits<int32_t>::max());
		m_dummydata.set_alt(std::numeric_limits<int32_t>::max());
		return m_dummydata;
	}
	return m_data[lat * m_lonsize + lon];
}

const DriftDown::GridPt& DriftDown::operator()(int lat, int lon) const
{
	if (lat < 0 || lon < 0 || lat >= m_latsize || lon >= m_lonsize)
		return dummydata;
	return m_data[lat * m_lonsize + lon];
}

DriftDown::GridPt& DriftDown::operator()(const TopoDb30::TopoCoordinate& tc)
{
	return operator()(m_topoorigin.lat_dist(tc), m_topoorigin.lon_dist(tc));
}

const DriftDown::GridPt& DriftDown::operator()(const TopoDb30::TopoCoordinate& tc) const
{
	return operator()(m_topoorigin.lat_dist(tc), m_topoorigin.lon_dist(tc));
}

Rect DriftDown::get_rect(void) const
{
	Point halfpt(TopoDb30::TopoCoordinate::get_pointsize());
	halfpt.set_lon(halfpt.get_lon() >> 1);
	halfpt.set_lat(halfpt.get_lat() >> 1);
	return Rect((Point)m_topoorigin - halfpt,
		    (Point)TopoDb30::TopoCoordinate(m_topoorigin.get_lon() + m_lonsize, m_topoorigin.get_lat() + m_latsize) - halfpt);
}

Point DriftDown::get_coord(int lat, int lon) const
{
	return TopoDb30::TopoCoordinate(m_topoorigin.get_lon() + lon, m_topoorigin.get_lat() + lat);
}

Point DriftDown::get_coord(const TopoDb30::TopoCoordinate& tc) const
{
	return TopoDb30::TopoCoordinate(m_topoorigin.get_lon() + tc.get_lon(), m_topoorigin.get_lat() + tc.get_lat());
}

DriftDown::minmaxalt_t DriftDown::get_minmaxalt(void) const
{
	minmaxalt_t r(std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::min());
	for (int lat = 0; lat < m_latsize; ++lat) {
		for (int lon = 0; lon < m_lonsize; ++lon) {
			int32_t alt(operator()(lat, lon).get_alt());
			if (alt == std::numeric_limits<int32_t>::max())
				continue;
			r.first = std::min(r.first, alt);
			r.second = std::max(r.second, alt);
		}
	}
	return r;
}

DriftDown::wx_t DriftDown::get_wx(int32_t alt) const
{
	wx_t r;
	for (unsigned int i = 1; i < nrweather; ++i) {
		if (alt < GRIB2::WeatherProfilePoint::altitudes[i - 1]) {
			r.first.set_wind_grib_mps(m_wind[i - 1]);
			r.second = m_temp[i - 1];
			break;
		}
		if (alt > GRIB2::WeatherProfilePoint::altitudes[i]) {
			if (i + 1 < nrweather)
				continue;
			r.first.set_wind_grib_mps(m_wind[i]);
			r.second = m_temp[i];
			break;
		}
		double frac((alt - GRIB2::WeatherProfilePoint::altitudes[i - 1]) /
			    (GRIB2::WeatherProfilePoint::altitudes[i] - GRIB2::WeatherProfilePoint::altitudes[i - 1]));
		gribwind_t w0(m_wind[i - 1]);
		gribwind_t wd(m_wind[i]);
		wd.first -= w0.first;
		wd.second -= w0.second;
		r.first.set_wind_grib_mps(gribwind_t(w0.first + wd.first * frac, w0.second + wd.second * frac));
		float t0(m_temp[i - 1]);
		float td(m_temp[i] - t0);
		r.second = t0 + td * frac;
		break;
	}
	return r;
}

int32_t DriftDown::compute_startalt(const Aircraft::ClimbDescent& cd, const Point& p0, const Point& p1, int32_t alt1) const
{
	static constexpr double nmi_to_ft = 1.0 / Point::km_to_nmi_dbl * 1e3 * Point::m_to_ft_dbl;
	static constexpr double maxglide = 0;
	std::pair<Point,double> gcnav(p0.get_gcnav(p1, 0.5));
	wx_t wx(get_wx(alt1));
	if (alt1 >= cd.get_ceiling())
		return std::numeric_limits<int32_t>::max();
	double t1(cd.altitude_to_time(alt1));
	wx.first.set_crs_tas(gcnav.second, cd.time_to_tas(t1));
	double d1(cd.time_to_distance(t1));
	double dist(p0.spheric_distance_nmi_dbl(p1));
	double d0(d1 + dist * wx.first.get_tas() / wx.first.get_gs());
	double t0(cd.distance_to_time(d0));
	double a0(cd.time_to_altitude(t0));
	if (maxglide > 0)
		a0 = std::max(a0, alt1 + dist * (nmi_to_ft / maxglide));
	if (std::isnan(a0) || a0 >= cd.get_ceiling())
		return std::numeric_limits<int32_t>::max();
	int32_t alt0(Point::round<int32_t,double>(a0));
	alt0 = std::max(alt0, alt1);
	if (false)
		std::cerr << "DD: " << p0.get_lat_str2() << ' ' << p0.get_lon_str2() << ' ' << alt0 << "ft -> "
			  << p1.get_lat_str2() << ' ' << p1.get_lon_str2() << ' ' << alt1 << "ft" << std::endl
			  << "  t0 " << t0 << " d0 " << d0 << " a0 " << a0 << " t1 " << t1 << " d1 " << d1 << " qnh " << m_qnh
			  << " temp " << wx.second << " wind " << wx.first.get_winddir() << '/' << wx.first.get_windspeed() << std::endl;
	return alt0;
}

void DriftDown::calculate(queue_t& queue, const Aircraft::ClimbDescent& cd, int latc, int lonc, const Point& p1, int32_t alt1, int32_t terrainheight)
{
	for (int lon = -2; lon <= 2; ++lon) {
		for (int lat = -2; lat <= 2; ++lat) {
			if (!(lon | lat))
				continue;
			int latp(lat + latc);
			int lonp(lon + lonc);
			if (latp < 0 || latp >= m_latsize || lonp < 0 || lonp >= m_lonsize)
				continue;
			if (abs(lon) >= 2 || abs(lat) >= 2) {
				int32_t altx(alt1 - terrainheight);
				if (operator()(latc + (lat >> 1), lonc + (lon >> 1)).get_elev() > altx)
					continue;
				if (operator()(latc + ((lat + 1) >> 1), lonc + ((lon + 1) >> 1)).get_elev() > altx)
					continue;
			}
			Point p0(get_coord(latp, lonp));
			GridPt& gpt(operator()(latp, lonp));
			int32_t alt0(compute_startalt(cd, p0, p1, alt1));
			if (alt0 == std::numeric_limits<int32_t>::max())
				continue;
			int32_t terrain(gpt.get_elev());
			terrain = std::min(terrain, std::numeric_limits<int32_t>::max() - terrainheight) + terrainheight;
			alt0 = std::max(alt0, terrain);
			if (alt0 >= gpt.get_alt())
				continue;
			if (gpt.get_alt() != std::numeric_limits<int32_t>::max()) {
				queue_t::iterator i(queue.find(QueuePt(latp, lonp, gpt.get_alt())));
				if (i != queue.end())
					queue.erase(i);
				else if (!false)
					std::cerr << "DriftDown::calculate: queue point not found for "
						  << latp << ',' << lonp << ',' << gpt.get_alt() << std::endl;
			}
			gpt.set_alt(alt0);
			if (!queue.insert(QueuePt(latp, lonp, gpt.get_alt())).second && !false)
				std::cerr << "DriftDown::calculate: duplicate queue point for "
					  << latp << ',' << lonp << ',' << gpt.get_alt() << std::endl;
		}
	}
}

#ifdef HAVE_EIGEN3

namespace {

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif
Eigen::VectorXd least_squares_solution(const Eigen::MatrixXd& A, const Eigen::VectorXd& b)
{
	if (false) {
		// traditional
		return (A.transpose() * A).inverse() * A.transpose() * b;
	} else if (true) {
		// cholesky
		return (A.transpose() * A).ldlt().solve(A.transpose() * b);
	} else if (false) {
		// householder QR
		return A.colPivHouseholderQr().solve(b);
	} else if (false) {
		// jacobi SVD
		return A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);
	} else {
		typedef Eigen::JacobiSVD<Eigen::MatrixXd, Eigen::FullPivHouseholderQRPreconditioner> jacobi_t;
		jacobi_t jacobi(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
		return jacobi.solve(b);
	}
}

};

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif
DriftDown::Neighbor *DriftDown::calculate(Neighbor *neigh, const Aircraft::ClimbDescent& cd) const
{
	static constexpr int32_t maxalt = 60000;
	static constexpr int32_t altinc = 500;
	static constexpr unsigned int polyorder = 5;
	if (!neigh)
		return 0;
	int latc(m_latsize >> 1), lonc(m_lonsize >> 1);
	Point pc(get_coord(latc, lonc));
	for (int lon = -2; lon <= 2; ++lon) {
		for (int lat = -2; lat <= 2; ++lat) {
			if (!lon && !lat)
				continue;
			*neigh = Neighbor(lat, lon);
			Point p0(get_coord(latc + lat, lonc + lon));
			unsigned int mp(maxalt / altinc + 1);
			Eigen::MatrixXd m(mp, polyorder);
			Eigen::VectorXd v(mp);
			int32_t ma(std::numeric_limits<int32_t>::min());
			mp = 0;
			for (int32_t alt = 0; alt <= maxalt; alt += altinc, ++mp) {
				int32_t alt0(compute_startalt(cd, p0, pc, alt));
				if (alt0 == std::numeric_limits<int32_t>::max())
					break;
				double a1(1);
				for (unsigned int i = 0; i < polyorder; ++i, a1 *= alt)
					m(mp, i) = a1;
				v(mp) = alt0;
				ma = alt;
			}
			if (!mp)
				continue;
			m.conservativeResize(mp, polyorder);
			v.conservativeResize(mp);
			Eigen::VectorXd x(least_squares_solution(m, v));
			Neighbor::poly_t p(x.data(), x.data() + x.size());
			neigh->set_altpoly(p, ma);
			// assess delta error
			if (false) {
				double err(0);
				for (unsigned int i = 0; i < mp; ++i) {
					int32_t e(neigh->get_alt(m(i, 1)) - v(i));
					err += e * e;
				}
				err /= mp;
				err = sqrt(err);
				std::cerr << "Neighbor poly approx error (" << lat << ',' << lon << "): " << err << "ft (1sigma)" << std::endl;
			}
			++neigh;
		}
	}
	return neigh;
}

#endif

void DriftDown::calculate(queue_t& queue, const Neighbor *neigh, const Neighbor *neighend, int latc, int lonc, int32_t alt1, int32_t terrainheight)
{
	for (; neigh != neighend; ++neigh) {
		int lat(neigh->get_latdisp()), lon(neigh->get_londisp());
		int latp(lat + latc);
		int lonp(lon + lonc);
		if (latp < 0 || latp >= m_latsize || lonp < 0 || lonp >= m_lonsize)
			continue;
		if (abs(lon) >= 2 || abs(lat) >= 2) {
			int32_t altx(alt1 - terrainheight);
			if (operator()(latc + (lat >> 1), lonc + (lon >> 1)).get_elev() > altx)
				continue;
			if (operator()(latc + ((lat + 1) >> 1), lonc + ((lon + 1) >> 1)).get_elev() > altx)
				continue;
		}
		GridPt& gpt(operator()(latp, lonp));
		int32_t alt0(neigh->get_alt(alt1));
		if (alt0 == std::numeric_limits<int32_t>::max())
			continue;
		int32_t terrain(gpt.get_elev());
		terrain = std::min(terrain, std::numeric_limits<int32_t>::max() - terrainheight) + terrainheight;
		alt0 = std::max(alt0, terrain);
		if (alt0 >= gpt.get_alt())
			continue;
		if (gpt.get_alt() != std::numeric_limits<int32_t>::max()) {
			queue_t::iterator i(queue.find(QueuePt(latp, lonp, gpt.get_alt())));
			if (i != queue.end())
				queue.erase(i);
			else if (!false)
				std::cerr << "DriftDown::calculate: queue point not found for "
					  << latp << ',' << lonp << ',' << gpt.get_alt() << std::endl;
		}
		gpt.set_alt(alt0);
		if (!queue.insert(QueuePt(latp, lonp, gpt.get_alt())).second && !false)
			std::cerr << "DriftDown::calculate: duplicate queue point for "
				  << latp << ',' << lonp << ',' << gpt.get_alt() << std::endl;
	}
}

void DriftDown::calculate(const Aircraft::ClimbDescent& cd, int32_t arrheight, int32_t terrainheight, bool exact)
{
	queue_t queue;
	// set up start
	{
		int32_t el(m_elev);
		if (el == std::numeric_limits<int32_t>::min())
			el = operator()(m_latsize >> 1, m_lonsize >> 1).get_elev();
		el += arrheight;
		calculate(queue, cd, m_latsize >> 1, m_lonsize >> 1, m_coord, el, terrainheight);
	}
	// process queue until empty
#ifdef HAVE_EIGEN3
	if (!exact) {
		Neighbor neigh[24];
		Neighbor *neighend(calculate(neigh, cd));
		while (!queue.empty()) {
			int latc, lonc;
			{
				const QueuePt& qp(*queue.begin());
				latc = qp.get_lat();
				lonc = qp.get_lon();
			}
			queue.erase(queue.begin());
			calculate(queue, neigh, neighend, latc, lonc, operator()(latc, lonc).get_alt(), terrainheight);
		}
	} else
#endif
	while (!queue.empty()) {
		int latc, lonc;
		{
			const QueuePt& qp(*queue.begin());
			latc = qp.get_lat();
			lonc = qp.get_lon();
		}
		queue.erase(queue.begin());
		calculate(queue, cd, latc, lonc, get_coord(latc, lonc), operator()(latc, lonc).get_alt(), terrainheight);
	}
	// fill border
	for (int lat = 0; lat < m_latsize; ++lat) {
		operator()(lat, 0).set_alt(std::numeric_limits<int32_t>::max());
		operator()(lat, m_lonsize - 1).set_alt(std::numeric_limits<int32_t>::max());
	}
	for (int lon = 0; lon < m_lonsize; ++lon) {
		operator()(0, lon).set_alt(std::numeric_limits<int32_t>::max());
		operator()(m_latsize - 1, lon).set_alt(std::numeric_limits<int32_t>::max());
	}
	if (false) {
		std::cerr << "DriftDown for " << m_coord.get_lat_str2() << ' ' << m_coord.get_lon_str2() << ' '
			  << m_elev << "ft (" << m_latsize << 'x' << m_lonsize << ')';
		for (int lat = 0; lat < m_latsize; ++lat) {
			for (int lon = 0; lon < m_lonsize; ++lon) {
				if (!(lon & 7))
					std::cerr << std::endl << '[' << lat << ',' << lon << "]:";
				const GridPt& gpt(operator()(lat, lon));
				std::cerr << ' ' << gpt.get_elev() << ',' << gpt.get_alt();
			}
		}
		std::cerr << std::endl;
	}
}

// contour tracing algorithms
// http://www.imageprocessingplace.com/downloads_V3/root_downloads/tutorials/contour_tracing_Abeer_George_Ghuneim/theo.html

Point DriftDown::interpolate(const Point& p0, const Point& p1, double frac)
{
	Point px(p1 - p0);
	px.set_lat(Point::round<Point::coord_t,double>(px.get_lat() * frac));
	px.set_lon(Point::round<Point::coord_t,double>(px.get_lon() * frac));
	return p0 + px;
}

double DriftDown::interpolate_factor(int32_t alt0, int32_t alt1, int32_t alt)
{
	if (alt0 == alt1)
		return 0.5;
	return (alt - alt0) / (double)(alt1 - alt0);
}

Point DriftDown::interpolate(const Point& p0, int32_t alt0, const Point& p1, int32_t alt1, int32_t alt)
{
	return interpolate(p0, p1, interpolate_factor(alt0, alt1, alt));
}

unsigned int DriftDown::get_index(unsigned int x, unsigned int y) const
{
	if (x >= m_latsize || y >= m_lonsize)
		return ~0U;
	return y * m_latsize + x;
}

unsigned int DriftDown::get_edgenr(unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1) const
{
      if ((x0 != x1 && y0 != y1) ||
          (x0 == x1 && y0 == y1))
              throw std::runtime_error("Grid::get_edgenr");
      if (x0 > x1)
              std::swap(x0, x1);
      if (y0 > y1)
              std::swap(y0, y1);
      if (x1 - x0 > 1 || y1 - y0 > 1)
              throw std::runtime_error("Grid::get_edgenr");
      return (get_index(x0, y0) << 1) | (y1 > y0);
}

void DriftDown::calculate(edges_t& edges, int32_t alt) const
{
	int width(m_latsize), height(m_lonsize);
	if (width < 2 || height < 2)
		return;
	width -= 2;
	height -= 2;
	for (int x(0); x <= width; ++x)
		for (int y(0); y <= height; ++y) {
			// Ordering of Points:
			// 0 1
			// 3 2
			unsigned int xx[4], yy[4];
			for (unsigned int i = 0; i < 4; ++i) {
				xx[i] = x + ((i ^ (i >> 1)) & 1);
				yy[i] = y + ((i >> 1) & 1);
			}
			for (unsigned int i = 0; i < 4; ++i) {
				int32_t alt0(operator()(xx[i & 3], yy[i & 3]).get_alt());
				int32_t alt1(operator()(xx[(i + 1) & 3], yy[(i + 1) & 3]).get_alt());
				int32_t alt2(operator()(xx[(i + 2) & 3], yy[(i + 2) & 3]).get_alt());
				int32_t alt3(operator()(xx[(i + 3) & 3], yy[(i + 3) & 3]).get_alt());
				Point pt0(get_coord(xx[i & 3], yy[i & 3]));
				Point pt1(get_coord(xx[(i + 1) & 3], yy[(i + 1) & 3]));
				Point pt2(get_coord(xx[(i + 2) & 3], yy[(i + 2) & 3]));
				Point pt3(get_coord(xx[(i + 3) & 3], yy[(i + 3) & 3]));
				if (i < 2 && alt0 < alt && alt1 >= alt && alt2 < alt && alt3 >= alt) {
					int32_t altm(((int64_t)alt0 + alt1 + alt2 + alt3 + 2) >> 2);
					Point ptm(pt0.halfway(pt2));
					if (altm >= alt) {
						edges.insert(Edge(interpolate(pt3, alt3, pt0, alt0, alt), get_edgenr(xx[(i + 3) & 3], yy[(i + 3) & 3], xx[i & 3], yy[i & 3]),
								  interpolate(pt1, alt1, pt0, alt0, alt), get_edgenr(xx[(i + 1) & 3], yy[(i + 1) & 3], xx[i & 3], yy[i & 3]),
								  interpolate(ptm, altm, pt0, alt0, alt)));
						edges.insert(Edge(interpolate(pt1, alt1, pt2, alt2, alt), get_edgenr(xx[(i + 1) & 3], yy[(i + 1) & 3], xx[(i + 2) & 3], yy[(i + 2) & 3]),
								  interpolate(pt3, alt3, pt2, alt2, alt), get_edgenr(xx[(i + 3) & 3], yy[(i + 3) & 3], xx[(i + 2) & 3], yy[(i + 2) & 3]),
								  interpolate(ptm, altm, pt2, alt2, alt)));
						break;
					}
					edges.insert(Edge(interpolate(pt1, alt1, pt2, alt2, alt), get_edgenr(xx[(i + 1) & 3], yy[(i + 1) & 3], xx[(i + 2) & 3], yy[(i + 2) & 3]),
							  interpolate(pt1, alt1, pt0, alt0, alt), get_edgenr(xx[(i + 1) & 3], yy[(i + 1) & 3], xx[i & 3], yy[i & 3]),
							  interpolate(pt1, alt1, ptm, altm, alt)));
					edges.insert(Edge(interpolate(pt3, alt3, pt0, alt0, alt), get_edgenr(xx[(i + 3) & 3], yy[(i + 3) & 3], xx[i & 3], yy[i & 3]),
							  interpolate(pt3, alt3, pt2, alt2, alt), get_edgenr(xx[(i + 3) & 3], yy[(i + 3) & 3], xx[(i + 2) & 3], yy[(i + 2) & 3]),
							  interpolate(pt3, alt3, ptm, altm, alt)));
					break;
				}
				if (alt0 < alt && alt1 >= alt && alt2 >= alt && alt3 >= alt) {
					edges.insert(Edge(interpolate(pt3, alt3, pt0, alt0, alt), get_edgenr(xx[(i + 3) & 3], yy[(i + 3) & 3], xx[i & 3], yy[i & 3]),
							  interpolate(pt1, alt1, pt0, alt0, alt), get_edgenr(xx[(i + 1) & 3], yy[(i + 1) & 3], xx[i & 3], yy[i & 3]),
							  interpolate(pt2, alt2, pt0, alt0, alt)));
					break;
				}
				if (alt0 < alt && alt1 < alt && alt2 >= alt && alt3 >= alt) {
					edges.insert(Edge(interpolate(pt3, alt3, pt0, alt0, alt), get_edgenr(xx[(i + 3) & 3], yy[(i + 3) & 3], xx[i & 3], yy[i & 3]),
							  interpolate(pt2, alt2, pt1, alt1, alt), get_edgenr(xx[(i + 2) & 3], yy[(i + 2) & 3], xx[(i + 1) & 3], yy[(i + 1) & 3])));
					break;
				}
				if (alt0 < alt && alt1 < alt && alt2 < alt && alt3 >= alt) {
					edges.insert(Edge(interpolate(pt0, alt0, pt3, alt3, alt), get_edgenr(xx[i & 3], yy[i & 3], xx[(i + 3) & 3], yy[(i + 3) & 3]),
							  interpolate(pt2, alt2, pt3, alt3, alt), get_edgenr(xx[(i + 2) & 3], yy[(i + 2) & 3], xx[(i + 3) & 3], yy[(i + 3) & 3]),
							  interpolate(pt1, alt1, pt3, alt3, alt)));
					break;
				}
			}
		}
}

PolygonSimple DriftDown::extract_contour(edges_t& edges)
{
	if (edges.empty())
		return PolygonSimple();
	std::vector<Edge> extredges;
	extredges.push_back(*edges.begin());
	edges.erase(edges.begin());
	if (false)
		std::cerr << "Edge: " << extredges.back().get_startedge() << " -> " << extredges.back().get_endedge()
			  << ' ' << extredges.back().get_start().get_lat_str2() << ' ' << extredges.back().get_start().get_lon_str2()
			  << " -> " << extredges.back().get_end().get_lat_str2() << ' ' << extredges.back().get_end().get_lon_str2()
			  << " (*)" << std::endl;
	for (;;) {
		edges_t::iterator i(edges.lower_bound(Edge(Point::invalid, extredges.back().get_endedge(), Point::invalid, 0)));
		if (i == edges.end() || i->get_startedge() != extredges.back().get_endedge())
			break;
		extredges.push_back(*i);
		edges.erase(i);
		if (false)
			std::cerr << "Edge: " << extredges.back().get_startedge() << " -> " << extredges.back().get_endedge()
				  << ' ' << extredges.back().get_start().get_lat_str2() << ' ' << extredges.back().get_start().get_lon_str2()
				  << " -> " << extredges.back().get_end().get_lat_str2() << ' ' << extredges.back().get_end().get_lon_str2() << std::endl;
	}
	PolygonSimple p;
	for (std::vector<Edge>::const_iterator i(extredges.begin()), e(extredges.end()); i != e; ++i) {
		p.push_back(i->get_start());
		if (!i->get_intermediate().is_invalid())
			p.push_back(i->get_intermediate());
	}
	if (extredges.front().get_startedge() != extredges.back().get_endedge())
		std::cerr << "DriftDown::extract_contour: warning: non-closed contour" << std::endl;
	else
		p.resize(p.size() - 1);
	return p;
}

MultiPolygonHole DriftDown::get_contour(int32_t alt) const
{
	MultiPolygonHole r;
	edges_t edges;
	calculate(edges, alt);
	while (!edges.empty()) {
		PolygonSimple ps(extract_contour(edges));
		if (ps.empty())
			break;
		if (ps.size() < 3)
			continue;
		r.push_back(PolygonHole(ps));
		if (false)
			std::cerr << "DriftDown::extract_contour: new contour area2 " << ps.area2() << std::endl;
	}
	if (!r.empty()) {
		r.geos_union(MultiPolygonHole());
		if (false)
			for (MultiPolygonHole::const_iterator mpi(r.begin()), mpe(r.end()); mpi != mpe; ++mpi) {
				std::cerr << "DriftDown::extract_contour: result outer contour area2 " << mpi->get_exterior().area2() << std::endl;
				for (unsigned int i(0), n(mpi->get_nrinterior()); i < n; ++i)
					std::cerr << "DriftDown::extract_contour: result inner contour area2 " << mpi->operator[](i).area2() << std::endl;
			}
	}
	return r;
}

bool DriftDown::update_profile(DriftDownProfile& prof, const std::string& sitename)
{
	bool ret(false);
	Rect bbox(get_rect());
	for (DriftDownProfile::iterator pi(prof.begin()), pe(prof.end()); pi != pe; ++pi) {
		if (!bbox.is_inside(pi->get_pt()))
			continue;
		const GridPt& gpt(operator()(TopoDb30::TopoCoordinate(pi->get_pt())));
		ret = pi->add_glide(gpt.get_alt(), sitename) || ret;
	}
	return ret;
}

DriftDownChart::Site::Site(const std::string& name, const Point& coord, int32_t elev, const std::string& comment)
	: m_name(name), m_comment(comment), m_coord(coord), m_elev(elev)
{
}

DriftDownChart::Site::Site(const DbBaseElements::Airport& arpt)
	: m_name(), m_comment(), m_coord(Point::invalid), m_elev(std::numeric_limits<int32_t>::min())
{
	set(arpt);
}

void DriftDownChart::Site::set(const DbBaseElements::Airport& arpt)
{
	m_name.clear();
	m_coord.set_invalid();
	m_elev = std::numeric_limits<int32_t>::min();
	if (!arpt.is_valid())
		return;
	set_name(arpt.get_icao());
	set_coord(arpt.get_coord());
	set_elev(arpt.get_elevation());
	std::ostringstream comment;
	for (unsigned int rwyc(0), rwyn(arpt.get_nrrwy()); rwyc < rwyn; ++rwyc) {
           	const DbBaseElements::Airport::Runway& rwy(arpt.get_rwy(rwyc));
		if (!rwy.is_he_usable() && !rwy.is_le_usable())
			continue;
		comment << 'R';
		if (rwy.is_he_usable())
			comment << rwy.get_ident_he();
		if (rwy.is_he_usable() && rwy.is_le_usable())
			comment << '/';
		if (rwy.is_le_usable())
			comment << rwy.get_ident_le();
		comment << ' ' << Point::round<int32_t,float>(rwy.get_length() * Point::ft_to_m) << 'x'
			<< Point::round<int32_t,float>(rwy.get_width() * Point::ft_to_m) << ' ' << rwy.get_surface() << ' ';
	}
	set_comment(comment.str());
}

bool DriftDownChart::Site::parse(const std::string& x)
{
	{
		const char *cp(x.c_str());
		char *cp1;
		double lat(strtod(cp, &cp1));
		if (cp1 != cp && *cp1 == ',') {
			cp = cp1 + 1;
			double lon(strtod(cp, &cp1));
			if (cp1 != cp && (*cp1 == ',' || !*cp1)) {
				m_name.clear();
				m_coord.set_lat_deg_dbl(lat);
				m_coord.set_lon_deg_dbl(lon);
				m_elev = std::numeric_limits<int32_t>::min();
				if (*cp1 == ',') {
					cp = cp1 + 1;
					int32_t el(strtol(cp, &cp1, 0));
					if (cp1 != cp && (*cp1 == ',' || !*cp1)) {
						m_elev = el;
						if (*cp1 == ',')
							m_name = cp1 + 1;
					}
				}
				return true;
			}
		}
	}
	{
		std::string::size_type n(x.find(','));
		Point pt;
		if (!((Point::setstr_lat | Point::setstr_lon) & ~pt.set_str(x.substr(0, n)))) {
			m_name.clear();
			m_coord = pt;
			m_elev = std::numeric_limits<int32_t>::min();
			if (n != std::string::npos) {
				const char *cp(x.c_str() + n + 1);
				char *cp1;
				int32_t el(strtol(cp, &cp1, 0));
				if (cp1 != cp && (*cp1 == ',' || !*cp1)) {
					m_elev = el;
					if (*cp1 == ',')
						m_name = cp1 + 1;
				}
			}
			return true;
		}
	}
	return false;
}

bool DriftDownChart::Site::is_name_alphanum(void) const
{
	if (m_name.empty())
		return false;
	for (std::string::const_iterator i(m_name.begin()), e(m_name.end()); i != e; ++i)
		if (!std::isalnum(*i))
			return false;
	return true;
}

DriftDownChart::Contour::Contour(int32_t alt, double width, uint8_t red, uint8_t green, uint8_t blue,
				 const std::string& tikzstroke,	uint8_t redfill, uint8_t greenfill, uint8_t bluefill,
				 const std::string& tikzfill)

	: m_tikzstroke(tikzstroke), m_tikzfill(tikzfill), m_width(width), m_alt(alt)
{
	m_rgbstroke[0] = red;
	m_rgbstroke[1] = green;
	m_rgbstroke[2] = blue;
	m_rgbfill[0] = redfill;
	m_rgbfill[1] = greenfill;
	m_rgbfill[2] = bluefill;
}

void DriftDownChart::Contour::add_poly(const MultiPolygonHole& p)
{
	m_poly.geos_union(p);
}

void DriftDownChart::Contour::get_stroke(uint8_t& red, uint8_t& green, uint8_t& blue) const
{
	red = m_rgbstroke[0];
	green = m_rgbstroke[1];
	blue = m_rgbstroke[2];
}

void DriftDownChart::Contour::get_stroke(double& red, double& green, double& blue) const
{
	static constexpr double scale = 1.0 / 255;
	red = m_rgbstroke[0] * scale;
	green = m_rgbstroke[1] * scale;
	blue = m_rgbstroke[2] * scale;
}

void DriftDownChart::Contour::get_fill(uint8_t& red, uint8_t& green, uint8_t& blue) const
{
	red = m_rgbfill[0];
	green = m_rgbfill[1];
	blue = m_rgbfill[2];
}

void DriftDownChart::Contour::get_fill(double& red, double& green, double& blue) const
{
	static constexpr double scale = 1.0 / 255;
	red = m_rgbfill[0] * scale;
	green = m_rgbfill[1] * scale;
	blue = m_rgbfill[2] * scale;
}


void DriftDownChart::Contour::set_stroke(const Cairo::RefPtr<Cairo::Context>& cr) const
{
	if (!cr)
		return;
	double r, g, b;
	get_stroke(r, g, b);
	cr->set_source_rgb(r, g, b);
	cr->set_line_width(get_width());
}

void DriftDownChart::Contour::set_fill(const Cairo::RefPtr<Cairo::Context>& cr) const
{
	if (!cr)
		return;
	double r, g, b;
	get_fill(r, g, b);
	cr->set_source_rgb(r, g, b);
}

std::string DriftDownChart::Contour::latex_strokecol(void) const
{
	std::ostringstream oss;
	oss << "coldriftdownstroke" << get_alt();
	return oss.str();
}

std::string DriftDownChart::Contour::latex_fillcol(void) const
{
	std::ostringstream oss;
	oss << "coldriftdownfill" << get_alt();
	return oss.str();
}

std::string DriftDownChart::Contour::define_latex_strokecol(void) const
{
	double r, g, b;
	get_stroke(r, g, b);
	std::ostringstream oss;
	oss << "\\definecolor{" << latex_strokecol() << "}{rgb}{" << std::fixed
	    << r << ',' << g << ',' << b << "}%" << std::endl;
	return oss.str();
}

std::string DriftDownChart::Contour::define_latex_fillcol(void) const
{
	double r, g, b;
	get_fill(r, g, b);
	std::ostringstream oss;
	oss << "\\definecolor{" << latex_fillcol() << "}{rgb}{" << std::fixed
	    << r << ',' << g << ',' << b << "}%" << std::endl;
	return oss.str();
}

constexpr double DriftDownChart::max_dist_from_route;
constexpr double DriftDownChart::chart_strip_length;

constexpr double DriftDownChart::tex_char_width;
constexpr double DriftDownChart::tex_char_height;

DriftDownChart::DriftDownChart(const FPlanRoute& route, const alternates_t& altn, const altroutes_t& altnfpl)
	: m_route(route), m_altn(altn), m_altnroutes(altnfpl)
{
}

void DriftDownChart::add_fir(const std::string& id, const MultiPolygonHole& poly)
{
	m_set.add_fir(id, poly);
}

Rect DriftDownChart::get_bbox(unsigned int wptidx0, unsigned int wptidx1) const
{
	Rect bbox(get_route().get_bbox(wptidx0, wptidx1));
	if (wptidx1 < get_route().get_nrwpt())
		return bbox;
	for (alternates_t::const_iterator ai(m_altn.begin()), ae(m_altn.end()); ai != ae; ++ai) {
		if (ai->get_coord().is_invalid())
			continue;
		bbox = bbox.add(ai->get_coord());
	}
	for (altroutes_t::const_iterator ai(m_altnroutes.begin()), ae(m_altnroutes.end()); ai != ae; ++ai) {
		Rect bbox2(ai->get_bbox());
		bbox = bbox.add(bbox2);
	}
	return bbox;
}

Rect DriftDownChart::get_bbox(void) const
{
	return get_bbox(0, get_route().get_nrwpt());
}

Rect DriftDownChart::get_extended_bbox(void) const
{
	return get_bbox().oversize_nmi(max_dist_from_route);
}

Rect DriftDownChart::get_extended_bbox(unsigned int wptidx0, unsigned int wptidx1) const
{
	return get_bbox(wptidx0, wptidx1).oversize_nmi(max_dist_from_route);
}

std::pair<time_t,time_t> DriftDownChart::get_timebound(void) const
{
	if (!get_route().get_nrwpt())
		return std::pair<time_t,time_t>(std::numeric_limits<time_t>::max(), std::numeric_limits<time_t>::min());
	std::pair<time_t,time_t> r(get_route()[0].get_time_unix(), get_route()[get_route().get_nrwpt()-1].get_flighttime());
	for (alternates_t::const_iterator ai(m_altn.begin()), ae(m_altn.end()); ai != ae; ++ai) {
		if (ai->get_coord().is_invalid())
			continue;
		r.second = std::max(r.second, (time_t)ai->get_flighttime());
	}
	r.second += r.first;
	return r;
}

DriftDownChart::DriftDownProfile DriftDownChart::calculate_profile(const FPlanRoute& route)
{
	double dist(0);
	DriftDownProfile prof;
	for (unsigned int wptnr = 1U, nrwpt = route.get_nrwpt(); wptnr < nrwpt; ++wptnr) {
		const FPlanWaypoint& wpt0(route[wptnr - 1U]);
		const FPlanWaypoint& wpt1(route[wptnr]);
		double distleg(wpt0.get_coord().spheric_distance_nmi_dbl(wpt1.get_coord()));
		gint64 timeorig(route[0].get_time_unix() + wpt0.get_flighttime());
		gint64 timediff(wpt1.get_flighttime() - (gint64)wpt0.get_flighttime());
		double tinc(1.0);
		if (distleg > 0)
			tinc = 1.0 / distleg;
		else if (timediff > 0)
			tinc = 600.0 / timediff;
		else
			continue;
		tinc = std::max(tinc, 1e-3);
		Point ptorig(wpt0.get_coord());
		Point ptdiff(wpt1.get_coord() - ptorig);
		int32_t altorig(wpt0.get_altitude());
		int32_t altdiff(wpt1.get_altitude() - wpt0.get_altitude());
		for (double t = 0;; t += tinc) {
			if (t >= 1.0) {
				if (wptnr + 1 < nrwpt)
					break;
				t = 1.0;
			}
			if (false)
				std::cerr << "DD profile: wpt " << wptnr << '/' << nrwpt << " t " << t << std::endl;
			Point pt(Point(ptdiff.get_lon() * t, ptdiff.get_lat() * t) + ptorig);
			gint64 efftime(timeorig + t * timediff);
			int32_t alt(altorig + t * altdiff);
			bool first(wptnr == 1U && t == 0);
			double ldist(t * distleg);
			double rdist(dist + ldist);
			unsigned int wnr(wptnr - 1);
			if (t == 1.0) {
				ldist = 0;
				++wnr;
			}
			prof.push_back(DriftDownProfilePoint(ldist, rdist, wnr, pt, efftime, alt));
			if (t == 1.0)
				break;
		}
		dist += distleg;
	}
	return prof;
}

const FPlanWaypoint *DriftDownChart::find_nearest_fplanwpt(const Point& pt) const
{
	const FPlanWaypoint *nrst(0);
	if (pt.is_invalid())
		return nrst;
	for (unsigned int i(0), n(m_route.get_nrwpt()); i < n; ++i) {
		const FPlanWaypoint& wpt(m_route[i]);
		if (wpt.get_coord().is_invalid())
			continue;
		if (!nrst) {
			nrst = &wpt;
			continue;
		}
		if (pt.simple_distance_rel(wpt.get_coord()) >= pt.simple_distance_rel(nrst->get_coord()))
			continue;
		nrst = &wpt;
	}
	for (alternates_t::const_iterator ai(m_altn.begin()), ae(m_altn.end()); ai != ae; ++ai) {
		const FPlanWaypoint& wpt(*ai);
		if (wpt.get_coord().is_invalid())
			continue;
		if (!nrst) {
			nrst = &wpt;
			continue;
		}
		if (pt.simple_distance_rel(wpt.get_coord()) >= pt.simple_distance_rel(nrst->get_coord()))
			continue;
		nrst = &wpt;
	}
	for (altroutes_t::const_iterator ai(m_altnroutes.begin()), ae(m_altnroutes.end()); ai != ae; ++ai) {
		for (unsigned int i(0), n(ai->get_nrwpt()); i < n; ++i) {
			const FPlanWaypoint& wpt((*ai)[i]);
			if (wpt.get_coord().is_invalid())
				continue;
			if (!nrst) {
				nrst = &wpt;
				continue;
			}
			if (pt.simple_distance_rel(wpt.get_coord()) >= pt.simple_distance_rel(nrst->get_coord()))
				continue;
			nrst = &wpt;
		}
	}
	return nrst;
}

void DriftDownChart::calculate(TopoDb30& topodb, const GRIB2& wxdb, gint64 efftime, const Aircraft& acft,
			       int32_t arrheight, int32_t terrainheight)
{
	m_profile = calculate_profile(m_route);
	int32_t maxalt(std::numeric_limits<int32_t>::max());
	for (sites_t::iterator si(get_sites().begin()), se(get_sites().end()); si != se; ++si) {
		if (si->get_coord().is_invalid())
			continue;
		const FPlanWaypoint *nrst(find_nearest_fplanwpt(si->get_coord()));
		Aircraft::ClimbDescent cd(acft.calculate_glide("", nrst ? Aircraft::convert(Aircraft::unit_kg, acft.get_wb().get_massunit(), nrst->get_mass_kg()) : acft.get_mtom(),
							       nrst ? nrst->get_isaoffset_kelvin() : 0));
		double maxradius(cd.time_to_distance(cd.get_climbtime()));
		if (!false)
			std::cerr << "Calculate DriftDown " << si->get_name() << ' ' << si->get_coord().get_lat_str2() << ' '
				  << si->get_coord().get_lon_str2() << ' ' << si->get_elev() << "ft" << std::endl;
		DriftDown dd(si->get_coord(), si->get_elev());
		dd.load_topo(topodb, maxradius);
		if (si->get_elev() == std::numeric_limits<int32_t>::min()) {
			const DriftDown::GridPt& gpt(dd(TopoDb30::TopoCoordinate(si->get_coord())));
			if (gpt.get_elev() == std::numeric_limits<int32_t>::max())
				continue;
			si->set_elev(gpt.get_elev());
		}
		dd.load_wind(wxdb, efftime);
		dd.calculate(cd, arrheight, terrainheight);
		dd.update_profile(m_profile, si->get_name());
		DriftDown::minmaxalt_t minmax(dd.get_minmaxalt());
		if (minmax.first >= minmax.second)
			continue;
		int32_t maxalt1(std::numeric_limits<int32_t>::min());
		for (contours_t::iterator ci(get_contours().begin()), ce(get_contours().end()); ci != ce; ++ci) {
			if (ci->get_alt() <= minmax.first || ci->get_alt() >= minmax.second)
				continue;
			if (!false)
				std::cerr << "  DriftDown contour " << ci->get_alt() << std::endl;
			MultiPolygonHole mph(dd.get_contour(ci->get_alt()));
			if (mph.empty())
				continue;
			const_cast<Contour&>(*ci).add_poly(mph);
			maxalt1 = std::max(maxalt1, ci->get_alt());
		}
		if (maxalt1 != std::numeric_limits<int32_t>::min())
			maxalt = std::min(maxalt, maxalt1);
	}
	if (false)
		for (contours_t::iterator ci(get_contours().begin()), ce(get_contours().end()); ci != ce; ++ci)
			if (ci->get_alt() > maxalt)
				const_cast<Contour&>(*ci).set_poly(MultiPolygonHole());
}

void DriftDownChart::draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height)
{
	draw(0, m_route.get_nrwpt(), cr, width, height);
}

void DriftDownChart::draw(std::ostream& os, std::vector<std::string>& tempfiles, const std::string& tempdir, double width, double height)
{
	std::string tmpdir(tempdir);
	if (tmpdir.empty())
		tmpdir = Glib::get_tmp_dir();
	for (unsigned int wptidx1(0), wptn(get_route().get_nrwpt()); wptidx1 < wptn; ) {
                unsigned int wptidx0(wptidx1);
                //wptidx1 = get_chart_strip(wptidx0);
		wptidx1 = get_route().get_nrwpt();
		char filename[tmpdir.size() + 19];
		strcpy(filename, tmpdir.c_str());
		strcat(filename, "ddchart_XXXXXX.pdf");
		int fd(mkstemps(filename, 4));
		if (fd != -1) {
			{
				static constexpr double cm_to_pt(72.0 / 2.54);
				Cairo::RefPtr<Cairo::PdfSurface> pdfsurface(Cairo::PdfSurface::create(filename, width * cm_to_pt, height * cm_to_pt));
				Cairo::RefPtr<Cairo::Context> ctx(Cairo::Context::create(pdfsurface));
				draw(wptidx0, wptidx1, ctx, width * cm_to_pt, height * cm_to_pt);
				ctx->show_page();
				//surface->show_page();
			}
			close(fd);
			os << "\\begin{center}\n  \\includegraphics{" << filename << "}\n\\end{center}\n";
			tempfiles.push_back(filename);
		}
	}
}

void DriftDownChart::draw(unsigned int wptidx0, unsigned int wptidx1, const Cairo::RefPtr<Cairo::Context>& cr, int width, int height)
{
	if (wptidx1 < wptidx0 + 2 || m_route[wptidx0].get_coord().is_invalid() || m_route[wptidx1-1].get_coord().is_invalid())
		return;
	Rect bbox(get_extended_bbox(wptidx0, wptidx1));
	cr->save();
	// background
	cr->set_source_rgb(1.0, 1.0, 1.0);
	cr->paint();
	// clip
	cr->rectangle(0, 0, width, height);
	cr->clip();
	cr->translate(width * 0.5, height * 0.5);
	// scaling
	cr->select_font_face("sans", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);
	cr->set_font_size(12);
	//Point center(m_route[wptidx0].get_coord().halfway(m_route[wptidx1-1].get_coord()));
	Point center(bbox.boxcenter());
	double scalelon, scalelat;
	{
		Point p(bbox.get_northeast() - bbox.get_southwest());
		double lonscale(cos(center.get_lat_rad_dbl()));
		scalelat = std::max(p.get_lon() * lonscale / width, (double)p.get_lat() / height);
		scalelat = 1.0 / scalelat;
		scalelon = scalelat * lonscale;
		scalelat = -scalelat;
	}
	draw_bgndlayer(cr, bbox, center, scalelon, scalelat);
	// draw contours
	for (contours_t::const_reverse_iterator ci(get_contours().rbegin()), ce(get_contours().rend()); ci != ce; ++ci) {
		if (!(ci->is_fill() || ci->is_stroke()) || ci->get_poly().empty())
			continue;
		METARTAFChart::polypath(cr, ci->get_poly(), bbox, center, scalelon, scalelat);
		if (ci->is_fill()) {
			ci->set_fill(cr);
			if (ci->is_stroke()) {
				cr->fill_preserve();
				ci->set_stroke(cr);
				cr->stroke();
			}
		} else {
			ci->set_stroke(cr);
			cr->stroke();
		}
	}
	// draw route
	{
		std::set<std::string> filter;
		set_color_fplan(cr);
		cr->set_line_width(2);
		cr->begin_new_path();
		{
			bool first(true);
			for (unsigned int i(wptidx0); i < wptidx1; ++i) {
				const FPlanWaypoint& wpt(m_route[i]);
				if (wpt.get_coord().is_invalid())
					continue;
				double xc, yc;
				{
					Point pdiff(wpt.get_coord() - center);
					xc = pdiff.get_lon() * scalelon;
					yc = pdiff.get_lat() * scalelat;
				}
				if (first) {
					first = false;
					cr->move_to(xc, yc);
				} else {
					cr->line_to(xc, yc);
				}
			}
			if (!first)
				cr->stroke();
		}
	        if (wptidx1 >= m_route.get_nrwpt()) {
			{
				std::vector<double> dashes;
				dashes.push_back(2);
				dashes.push_back(2);
				cr->set_dash(dashes, 0);
			}
			for (alternates_t::const_iterator ai(m_altn.begin()), ae(m_altn.end()); ai != ae; ++ai) {
				if (ai->get_coord().is_invalid())
					continue;
				const FPlanWaypoint& wpte(m_route[m_route.get_nrwpt()-1]);
				if (wpte.get_coord().is_invalid())
					break;
				const FPlanRoute *falt(0);
				for (altroutes_t::const_iterator di(m_altnroutes.begin()), de(m_altnroutes.end()); di != de; ++di) {
					if (di->get_nrwpt() <= 0 || (*di)[di->get_nrwpt()-1].get_coord().is_invalid() ||
					    (*di)[di->get_nrwpt()-1].get_coord().simple_distance_nmi(ai->get_coord()) >= 1)
						continue;
					falt = &*di;
					break;
				}
				if (falt) {
					bool first(true);
					for (unsigned int i = 0, j = falt->get_nrwpt(); i < j; ++i) {
						const FPlanWaypoint& wpt((*falt)[i]);
						if (wpt.get_coord().is_invalid())
							continue;
						double xc, yc;
						{
							Point pdiff(wpt.get_coord() - center);
							xc = pdiff.get_lon() * scalelon;
							yc = pdiff.get_lat() * scalelat;
						}
						if (first) {
							first = false;
							cr->move_to(xc, yc);
						} else {
							cr->line_to(xc, yc);
						}
					}
					if (!first)
						cr->stroke();
				} else {
					double xc, yc;
					{
						Point pdiff(wpte.get_coord() - center);
						xc = pdiff.get_lon() * scalelon;
						yc = pdiff.get_lat() * scalelat;
					}
					cr->move_to(xc, yc);
					{
						Point pdiff(ai->get_coord() - center);
						xc = pdiff.get_lon() * scalelon;
						yc = pdiff.get_lat() * scalelat;
					}
					cr->line_to(xc, yc);
					cr->stroke();
				}
			}
			cr->unset_dash();
		}
		// use latex overpic environment to place labels?
		for (unsigned int i(wptidx0); i < wptidx1; ++i) {
			const FPlanWaypoint& wpt(m_route[i]);
			if (wpt.get_coord().is_invalid())
				continue;
			if (i && wpt.get_pathcode() == FPlanWaypoint::pathcode_sid)
				continue;
			if (i > 0 && i + 1 != m_route.get_nrwpt() && m_route[i-1].get_pathcode() == FPlanWaypoint::pathcode_star)
				continue;
			if (wpt.get_type() >= FPlanWaypoint::type_generated_start && wpt.get_type() <= FPlanWaypoint::type_generated_end)
				continue;
			Glib::ustring txt(wpt.get_icao());
			if (txt.empty() || (wpt.get_type() == FPlanWaypoint::type_airport && AirportsDb::Airport::is_fpl_zzzz(txt)))
				txt = wpt.get_name();
			if (txt.empty())
				continue;
			double xc, yc;
			{
				Point pdiff(wpt.get_coord() - center);
				xc = pdiff.get_lon() * scalelon;
				yc = pdiff.get_lat() * scalelat;
			}
			unsigned int quadrant(0);
			if (i)
				quadrant |= 1 << (((m_route[i-1].get_truetrack() + 0x8000) >> 14) & 3);
			if (i + 1 == m_route.get_nrwpt()) {
				for (alternates_t::const_iterator ai(m_altn.begin()), ae(m_altn.end()); ai != ae; ++ai) {
					if (ai->get_coord().is_invalid())
						continue;
					unsigned int q(wpt.get_coord().spheric_true_course_dbl(ai->get_coord()) * FPlanWaypoint::from_deg + 0x8000);
					quadrant |= 1 << ((q >> 14) & 3);
				}
			} else {
				quadrant |= 1 << ((wpt.get_truetrack() >> 14) & 3);
			}
			Cairo::TextExtents ext;
			cr->get_text_extents(txt, ext);
			xc -= ext.x_bearing;
			yc -= ext.y_bearing;
			// place label
			if (!(quadrant & 0x3)) {
				xc += 3;
				yc -= ext.height * 0.5;
			} else if (!(quadrant & 0x6)) {
				xc -= ext.width * 0.5;
				yc += 3;
			} else if (!(quadrant & 0xC)) {
				xc -= 3 + ext.width;
				yc -= ext.height * 0.5;
			} else if (!(quadrant & 0x9)) {
				xc -= ext.width * 0.5;
				yc -= 3 + ext.height;
			} else if (!(quadrant & 0x1)) {
				xc += 3;
				yc -= 3 + ext.height;
			} else if (!(quadrant & 0x2)) {
				xc += 3;
				yc += 3;
			} else if (!(quadrant & 0x4)) {
				xc -= 3 + ext.width;
				yc += 3;
			} else if (!(quadrant & 0x8)) {
				xc -= 3 + ext.width;
				yc -= 3 + ext.height;
			} else {
				xc -= ext.width * 0.5;
				yc -= ext.height * 0.5;
			}
			cr->set_source_rgb(1.0, 1.0, 1.0);
			cr->move_to(xc, yc + 1);
			cr->show_text(txt);
			cr->move_to(xc, yc - 1);
			cr->show_text(txt);
			cr->move_to(xc + 1, yc);
			cr->show_text(txt);
			cr->move_to(xc - 1, yc);
			cr->show_text(txt);
			set_color_fplan(cr);
			cr->move_to(xc, yc);
			cr->show_text(txt);
		}
		if (wptidx1 >= m_route.get_nrwpt()) {
			for (alternates_t::const_iterator ai(m_altn.begin()), ae(m_altn.end()); ai != ae; ++ai) {
				if (ai->get_coord().is_invalid())
					continue;
				const FPlanWaypoint& wpt(m_route[m_route.get_nrwpt()-1]);
				const FPlanWaypoint& wptp(m_route[m_route.get_nrwpt()-2]);
				if (wpt.get_coord() == ai->get_coord())
					continue;
				Glib::ustring txt(ai->get_icao());
				if (txt.empty() || AirportsDb::Airport::is_fpl_zzzz(txt))
					txt = ai->get_name();
				if (txt.empty())
					continue;
				double xc, yc;
				{
					Point pdiff(ai->get_coord() - center);
					xc = pdiff.get_lon() * scalelon;
					yc = pdiff.get_lat() * scalelat;
				}
				unsigned int quadrant(wpt.get_coord().spheric_true_course_dbl(ai->get_coord()) * FPlanWaypoint::from_deg + 0x8000);
				quadrant = 1 << ((quadrant >> 14) & 3);
				quadrant |= 1 << (((wptp.get_truetrack() + 0x8000) >> 14) & 3);
				Cairo::TextExtents ext;
				cr->get_text_extents(txt, ext);
				xc -= ext.x_bearing;
				yc -= ext.y_bearing;
				// place label
				if (!(quadrant & 0x3)) {
					xc += 3;
					yc -= ext.height * 0.5;
				} else if (!(quadrant & 0x6)) {
					xc -= ext.width * 0.5;
					yc += 3;
				} else if (!(quadrant & 0xC)) {
					xc -= 3 + ext.width;
					yc -= ext.height * 0.5;
				} else if (!(quadrant & 0x9)) {
					xc -= ext.width * 0.5;
					yc -= 3 + ext.height;
				} else if (!(quadrant & 0x1)) {
					xc += 3;
					yc -= 3 + ext.height;
				} else if (!(quadrant & 0x2)) {
					xc += 3;
					yc += 3;
				} else if (!(quadrant & 0x4)) {
					xc -= 3 + ext.width;
					yc += 3;
				} else if (!(quadrant & 0x8)) {
					xc -= 3 + ext.width;
					yc -= 3 + ext.height;
				} else {
					xc -= ext.width * 0.5;
					yc -= ext.height * 0.5;
				}
				cr->set_source_rgb(1.0, 1.0, 1.0);
				cr->move_to(xc, yc + 1);
				cr->show_text(txt);
				cr->move_to(xc, yc - 1);
				cr->show_text(txt);
				cr->move_to(xc + 1, yc);
				cr->show_text(txt);
				cr->move_to(xc - 1, yc);
				cr->show_text(txt);
				set_color_fplan(cr);
				cr->move_to(xc, yc);
				cr->show_text(txt);
			}
		}
		// draw sites
		for (sites_t::const_iterator si(get_sites().begin()), se(get_sites().end()); si != se; ++si) {
			if (!bbox.is_inside(si->get_coord()))
				continue;
			double xc(0), yc(0);
			{
				Point pdiff(si->get_coord() - center);
				xc += pdiff.get_lon() * scalelon;
				yc += pdiff.get_lat() * scalelat;
			}
			// draw triangle
			{
				static const double triangle[3][2] = {
					{  2.598076211353316,  1.5 },
					{ -2.598076211353316,  1.5 },
					{  0.000000000000000, -3.0 }
				};
				cr->set_source_rgb(1.0, 0.0, 0.0);
				cr->set_line_width(1.0);
				cr->move_to(xc + triangle[0][0], yc + triangle[0][1]);
				for (unsigned int i = 1; i < 3; ++i)
					cr->line_to(xc + triangle[i][0], yc + triangle[i][1]);
				cr->close_path();
				cr->stroke();
			}
			// draw label
			if (false) {
				Cairo::TextExtents ext;
				cr->get_text_extents(si->get_name(), ext);
				xc += -ext.width * 0.5 - ext.x_bearing;
				yc -= ext.height * 0.5 + ext.y_bearing;
				cr->set_source_rgb(1.0, 1.0, 1.0);
				cr->move_to(xc, yc + 1);
				cr->show_text(si->get_name());
				cr->move_to(xc, yc - 1);
				cr->show_text(si->get_name());
				cr->move_to(xc + 1, yc);
				cr->show_text(si->get_name());
				cr->move_to(xc - 1, yc);
				cr->show_text(si->get_name());
				cr->set_source_rgb(0.0, 0.0, 0.0);
				cr->move_to(xc, yc);
				cr->show_text(si->get_name());
			}
		}
	}
	// end
	cr->restore();
}

void DriftDownChart::draw_firs(std::ostream& os, METARTAFChart::LabelLayout& lbl, const Rect& bbox, const Point& center,
			      double width, double height, double scalelon, double scalelat)
{
	for (MetarTafSet::firs_t::iterator fi(m_set.get_firs().begin()), fe(m_set.get_firs().end()); fi != fe; ++fi) {
		MetarTafSet::FIR& fir(const_cast<MetarTafSet::FIR&>(*fi));
		MetarTafSet::FIR::sigmet_t& sigmets(fir.get_sigmet());
		for (MetarTafSet::FIR::sigmet_t::iterator si(sigmets.begin()), se(sigmets.end()); si != se; ) {
			const MetarTafSet::SIGMET& sigmet(*si);
			MetarTafSet::FIR::sigmet_t::iterator si1(si);
			++si;
			MultiPolygonHole poly(sigmet.get_poly());
			if (poly.empty()) {
				sigmets.erase(si1);
				continue;
			}
			poly.geos_intersect(PolygonHole(bbox));
			if (poly.empty()) {
				sigmets.erase(si1);
				continue;
			}
			os << "  % FIR " << fir.get_ident() << " sigmet " << sigmet.get_msg() << std::endl;
			METARTAFChart::polypath_fill(os, "fill=red,fill opacity=0.2,draw=red,thick,draw opacity=1.0",
						     poly, center, width, height, scalelon, scalelat);
			double xc, yc;
			std::string txt(fir.get_ident() + " " + sigmet.get_sequence());
			{
				Point pdiff(poly[0].get_exterior().centroid());
				if (pdiff.is_invalid())
					continue;
				pdiff -= center;
				xc = pdiff.get_lon() * scalelon + 0.5 * width;
				yc = pdiff.get_lat() * scalelat + 0.5 * height;
			}
			std::ostringstream oss;
			oss << "\\sigmetlabellink{" << txt << '}';
			lbl.push_back(METARTAFChart::Label(txt, oss.str(), xc, yc, METARTAFChart::Label::placement_center, false, false));
		}
	}
}

void DriftDownChart::draw(std::ostream& os, double width, double height)
{
	draw(0, m_route.get_nrwpt(), os, width, height);
}

unsigned int DriftDownChart::get_chart_strip(unsigned int wptstart) const
{
	const unsigned int n(m_route.get_nrwpt());
	if (wptstart >= n || chart_strip_length <= 0. || m_route[wptstart].get_coord().is_invalid())
		return n;
	unsigned int wptend(wptstart);
	for (unsigned int i(wptend + 1); i < n; ++i) {
		if (m_route[i].get_coord().is_invalid())
			continue;
		if (wptend == wptstart)
			wptend = i;
		if (m_route[wptstart].get_coord().spheric_distance_nmi_dbl(m_route[i].get_coord()) > chart_strip_length)
			break;
		wptend = i;
	}
	if (wptend == wptstart)
		return n;
	return wptend + 1U;
}

void DriftDownChart::draw(unsigned int wptidx0, unsigned int wptidx1, std::ostream& os, double width, double height)
{
	if (wptidx1 < wptidx0 + 2 || m_route[wptidx0].get_coord().is_invalid() || m_route[wptidx1-1].get_coord().is_invalid())
		return;
	Rect bbox(get_extended_bbox(wptidx0, wptidx1));
	//Point center(m_route[wptidx0].get_coord().halfway(m_route[wptidx1-1].get_coord()));
	Point center(bbox.boxcenter());
	double scalelon, scalelat;
	{
		Point p(bbox.get_northeast() - bbox.get_southwest());
		double lonscale(cos(center.get_lat_rad_dbl()));
		scalelat = std::max(p.get_lon() * lonscale / width, (double)p.get_lat() / height);
		scalelat = 1.0 / scalelat;
		scalelon = scalelat * lonscale;
		height = p.get_lat() * scalelat;
		width = p.get_lon() * scalelon;
	}
	os << "  \\begin{center}\\begin{tikzpicture}" << std::endl
	   << "    \\path[clip] (0,0) rectangle " << METARTAFChart::pgfcoord(width, height) << ';' << std::endl;
	if (true)
		os << "    \\georefstart{4326}" << std::endl
		   << "    \\georefbound{0}{0}" << std::endl
		   << "    \\georefbound" << METARTAFChart::georefcoord(width, 0) << std::endl
		   << "    \\georefbound" << METARTAFChart::georefcoord(width, height) << std::endl
		   << "    \\georefbound" << METARTAFChart::georefcoord(0, height) << std::endl
		   << "    \\georefmap" << METARTAFChart::georefcoord(bbox.get_southwest(), center, width, height, scalelon, scalelat) << std::endl
		   << "    \\georefmap" << METARTAFChart::georefcoord(bbox.get_southeast(), center, width, height, scalelon, scalelat) << std::endl
		   << "    \\georefmap" << METARTAFChart::georefcoord(bbox.get_northeast(), center, width, height, scalelon, scalelat) << std::endl
		   << "    \\georefmap" << METARTAFChart::georefcoord(bbox.get_northwest(), center, width, height, scalelon, scalelat) << std::endl
		   << "    \\georefend" << std::endl;
	draw_bgndlayer(os, bbox, center, width, height, scalelon, scalelat);
	METARTAFChart::LabelLayout lbl(width, height, 1.2 * 4 * tex_char_width, 1.2 * tex_char_height);
	draw_firs(os, lbl, bbox, center, width, height, scalelon, scalelat);
	// site hyperlinks
	for (sites_t::const_iterator si(get_sites().begin()), se(get_sites().end()); si != se; ++si) {
		if (!bbox.is_inside(si->get_coord()))
			continue;
		double xc, yc;
		{
			Point pdiff(si->get_coord() - center);
			xc = pdiff.get_lon() * scalelon + 0.5 * width;
			yc = pdiff.get_lat() * scalelat + 0.5 * height;
		}
		// hyperlink
		if (true && si->is_name_alphanum()) {
			std::ostringstream oss;
			os << "\\node[draw,align=center] at " << METARTAFChart::pgfcoord(xc, yc)
			   << " {\\hyperlink{lnkdriftdown" << si->get_name() << "}{\\phantom{\\rule{3pt}{3pt}}}};" << std::endl;
		}
	}
	// draw contours
	for (contours_t::const_reverse_iterator ci(get_contours().rbegin()), ce(get_contours().rend()); ci != ce; ++ci) {
		if (!(ci->is_fill() || ci->is_stroke()) || ci->get_poly().empty())
			continue;
		if (!ci->is_fill()) {
			std::string code("draw=" + ci->latex_strokecol());
			if (!ci->get_tikzstroke().empty())
				code += "," + ci->get_tikzstroke();
			METARTAFChart::polypath(os, code, ci->get_poly(), bbox, center, width, height, scalelon, scalelat);
			continue;
		}
		std::string code("fill=" + ci->latex_fillcol());
		if (!ci->get_tikzfill().empty())
			code += "," + ci->get_tikzfill();
		if (ci->is_stroke()) {
			code += ",draw=" + ci->latex_strokecol();
			if (!ci->get_tikzstroke().empty())
				code += "," + ci->get_tikzstroke();
		} else {
			code += ",draw=none";
		}
		METARTAFChart::polypath_fill(os, code, ci->get_poly(), center, width, height, scalelon, scalelat);
	}
	// draw route
	{
		std::set<std::string> filter;
		bool first(true);
		for (unsigned int i(wptidx0); i < wptidx1; ++i) {
			const FPlanWaypoint& wpt(m_route[i]);
			if (wpt.get_coord().is_invalid())
				continue;
			double xc, yc;
			{
				Point pdiff(wpt.get_coord() - center);
				xc = pdiff.get_lon() * scalelon + 0.5 * width;
				yc = pdiff.get_lat() * scalelat + 0.5 * height;
			}
			if (first) {
				os << "    \\path[draw,thick,colfplan] ";
				first = false;
			} else {
				os << " -- ";
			}
			os << METARTAFChart::pgfcoord(xc, yc);
			if (i && wpt.get_pathcode() == FPlanWaypoint::pathcode_sid)
				continue;
			if (i > 0 && i + 1 != m_route.get_nrwpt() && m_route[i-1].get_pathcode() == FPlanWaypoint::pathcode_star)
				continue;
			if (wpt.get_type() >= FPlanWaypoint::type_generated_start && wpt.get_type() <= FPlanWaypoint::type_generated_end)
				continue;
			Glib::ustring txt(wpt.get_icao());
			if (txt.empty() || (wpt.get_type() == FPlanWaypoint::type_airport && AirportsDb::Airport::is_fpl_zzzz(txt)))
				txt = wpt.get_name();
			if (txt.empty())
				continue;
			unsigned int quadrant(0);
			if (i)
				quadrant |= 1 << (((m_route[i-1].get_truetrack() + 0x8000) >> 14) & 3);
			if (i + 1 == m_route.get_nrwpt()) {
				for (alternates_t::const_iterator ai(m_altn.begin()), ae(m_altn.end()); ai != ae; ++ai) {
					if (ai->get_coord().is_invalid())
						continue;
					unsigned int q(wpt.get_coord().spheric_true_course_dbl(ai->get_coord()) * FPlanWaypoint::from_deg + 0x8000);
					quadrant |= 1 << ((q >> 14) & 3);
				}
			} else {
				quadrant |= 1 << ((wpt.get_truetrack() >> 14) & 3);
			}
			// place label: quadrants
			// 8 1
			// 4 2
			METARTAFChart::Label::placement_t placement(METARTAFChart::Label::placement_from_quadrant(quadrant));
			{
				std::ostringstream oss;
				oss << "\\driftdownstationlabel{" << txt << "}{colfplan}";
				lbl.push_back(METARTAFChart::Label(txt, oss.str(), xc, yc, placement, true, i == wptidx0 || (i + 1) >= wptidx1));
			}
		}
		if (!first)
			os << ';' << std::endl;
		if (wptidx1 >= m_route.get_nrwpt()) {
			for (alternates_t::const_iterator ai(m_altn.begin()), ae(m_altn.end()); ai != ae; ++ai) {
				if (ai->get_coord().is_invalid())
					continue;
				const FPlanWaypoint& wpte(m_route[m_route.get_nrwpt()-1]);
				if (wpte.get_coord().is_invalid())
					break;
				const FPlanRoute *falt(0);
				for (altroutes_t::const_iterator di(m_altnroutes.begin()), de(m_altnroutes.end()); di != de; ++di) {
					if (di->get_nrwpt() <= 0 || (*di)[di->get_nrwpt()-1].get_coord().is_invalid() ||
					    (*di)[di->get_nrwpt()-1].get_coord().simple_distance_nmi(ai->get_coord()) >= 1)
						continue;
					falt = &*di;
					break;
				}
				double xc, yc;
				if (falt) {
					bool first(true);
					for (unsigned int i = 0, j = falt->get_nrwpt(); i < j; ++i) {
						const FPlanWaypoint& wpt((*falt)[i]);
						if (wpt.get_coord().is_invalid())
							continue;
						{
							Point pdiff(wpt.get_coord() - center);
							xc = pdiff.get_lon() * scalelon + 0.5 * width;
							yc = pdiff.get_lat() * scalelat + 0.5 * height;
						}
						if (first) {
							os << "    \\path[draw,thick,colfplan,dash pattern=on 2pt off 2pt] ";
							first = false;
						} else {
							os << " -- ";
						}
						os << METARTAFChart::pgfcoord(xc, yc);
					}
					if (first)
						continue;
				} else {
					{
						Point pdiff(wpte.get_coord() - center);
						xc = pdiff.get_lon() * scalelon + 0.5 * width;
						yc = pdiff.get_lat() * scalelat + 0.5 * height;
					}
					os << "    \\path[draw,thick,colfplan,dash pattern=on 2pt off 2pt] " << METARTAFChart::pgfcoord(xc, yc);
					{
						Point pdiff(ai->get_coord() - center);
						xc = pdiff.get_lon() * scalelon + 0.5 * width;
						yc = pdiff.get_lat() * scalelat + 0.5 * height;
					}
					os << " -- " << METARTAFChart::pgfcoord(xc, yc);
				}
				if (wpte.get_coord() == ai->get_coord()) {
					os << ';' << std::endl;
					continue;
				}
				Glib::ustring txt(ai->get_icao());
				if (txt.empty() || AirportsDb::Airport::is_fpl_zzzz(txt))
					txt = ai->get_name();
				os << ';' << std::endl;
				if (txt.empty())
					continue;
				const FPlanWaypoint& wptp(m_route[m_route.get_nrwpt()-2]);
				unsigned int quadrant(wpte.get_coord().spheric_true_course_dbl(ai->get_coord()) * FPlanWaypoint::from_deg + 0x8000);
				quadrant = 1 << ((quadrant >> 14) & 3);
				quadrant |= 1 << (((wptp.get_truetrack() + 0x8000) >> 14) & 3);
				// place label: quadrants
				// 8 1
				// 4 2
				METARTAFChart::Label::placement_t placement(METARTAFChart::Label::placement_from_quadrant(quadrant));
				{
	        			std::ostringstream oss;
					oss << "\\driftdownstationlabel{" << txt << "}{colfplan}";
					lbl.push_back(METARTAFChart::Label(txt, oss.str(), xc, yc, placement, true, true));
				}
			}
		}
		lbl.remove_overlapping_labels();
	}
	// draw sites
	for (sites_t::const_iterator si(get_sites().begin()), se(get_sites().end()); si != se; ++si) {
		if (!bbox.is_inside(si->get_coord()))
			continue;
		double xc, yc;
		{
			Point pdiff(si->get_coord() - center);
			xc = pdiff.get_lon() * scalelon + 0.5 * width;
			yc = pdiff.get_lat() * scalelat + 0.5 * height;
		}
		// draw triangle
		{
			static const double triangle[3][2] = {
				{  0.0433012701892219, -0.025 },
				{ -0.0433012701892219, -0.025 },
				{  0.0000000000000000,  0.050 }
			};
			os << "    \\path[draw,thin,red] ";
			for (unsigned int i = 0; i < 3; ++i)
				os << METARTAFChart::pgfcoord(xc + triangle[i][0], yc + triangle[i][1]) << " -- ";
			os << "cycle;" << std::endl;
		}
		// draw label
		if (false) {
			std::ostringstream oss;
			oss << "\\driftdownstationlabel{" << si->get_name() << "}{black}";
			lbl.push_back(METARTAFChart::Label(si->get_name(), oss.str(), xc, yc, METARTAFChart::Label::placement_center, false, false));
		}
	}
	// output labels
	lbl.setk(1);
	if (!false)
		std::cerr << "force directed layout: a " << lbl.total_attractive_force()
			  << " r " << lbl.total_repulsive_force() << std::endl;
	for (double temp = 0.05; temp > 0.001; temp *= 0.75) {
		lbl.iterate(temp);
		if (!false)
			std::cerr << "force directed layout: temp " << temp << " a " << lbl.total_attractive_force()
				  << " r " << lbl.total_repulsive_force() << std::endl;
	}
	for (METARTAFChart::LabelLayout::const_iterator li(lbl.begin()), le(lbl.end()); li != le; ++li) {
		if (!li->is_fixed())
			continue;
		os << "    \\node[" << li->get_tikz_anchor() << "] at " << METARTAFChart::pgfcoord(li->get_anchorx(), li->get_anchory())
		   << " {" << li->get_code() << "};" << std::endl;
	}
	for (METARTAFChart::LabelLayout::const_iterator li(lbl.begin()), le(lbl.end()); li != le; ++li) {
		if (li->is_fixed())
			continue;
		os << "    \\node[" << li->get_tikz_anchor() << "] at " << METARTAFChart::pgfcoord(li->get_x(), li->get_y())
		   << " {" << li->get_code() << "};" << std::endl;
	}
	// end picture
	os << "  \\end{tikzpicture}\\end{center}" << std::endl << std::endl;
}

void DriftDownChart::draw_legend(std::ostream& os)
{
	os << ("\\small\n"
	       "\\begin{center}\n"
	       "  \\noindent\\begin{tabular}{|l|l||l|l||l|l||l|l|}\n"
	       "    \\hline\n");
	unsigned int pos(0);
	for (contours_t::const_iterator ci(get_contours().begin()), ce(get_contours().end()); ci != ce; ++ci) {
		if (ci->get_poly().empty() || !(ci->is_fill() || ci->is_stroke()))
			continue;
		if (!pos)
			os << "    ";
		else
			os << " & ";
		os << "\\begin{tikzpicture}[sty/.style={}]";
		if (ci->is_fill()) {
			std::string code("fill," + ci->latex_fillcol());
			if (!ci->get_tikzfill().empty())
				code += "," + ci->get_tikzfill();
			os << "\\path[" << code << "] (0,0) rectangle (1,0.2);";
		}
		if (ci->is_stroke()) {
			std::string code("draw," + ci->latex_strokecol());
			if (!ci->get_tikzstroke().empty())
				code += "," + ci->get_tikzstroke();
			os << "\\path[" << code << "] (0,0.1) -- (1,0.1);";
		}
		os << "\\end{tikzpicture} & " << ci->get_alt() << "ft";
		++pos;
		pos &= 3;
		if (!pos)
			os << "\\\\" << std::endl << "    \\hline" << std::endl;
	}
	while (pos) {
		os << " & & ";
		++pos;
		pos &= 3;
		if (!pos)
			os << "\\\\" << std::endl << "    \\hline" << std::endl;
	}
	os << ("  \\end{tabular}\n"
	       "\\end{center}\n");
}

void DriftDownChart::define_latex_colors(std::ostream& os)
{
	for (contours_t::const_iterator ci(get_contours().begin()), ce(get_contours().end()); ci != ce; ++ci) {
		if (ci->is_stroke())
			os << ci->define_latex_strokecol();
		if (ci->is_fill())
			os << ci->define_latex_fillcol();
	}
	static const char latex_defs[] =
		"\\newcommand\\driftdownstationlabel[2]{\\contour{white}{\\textcolor{#2}{\\ttfamily{}#1}}}\n";
	os << latex_defs;
}

void DriftDownChart::draw_sitelist(std::ostream& os)
{
	unsigned int cnt(0);
	for (sites_t::const_iterator si(get_sites().begin()), se(get_sites().end()); si != se; ++si) {
		if (!cnt)
			os << ("\\newlength{\\driftdowntblsitename}\n"
			       "\\newlength{\\driftdowntbllat}\n"
			       "\\newlength{\\driftdowntbllon}\n"
			       "\\newlength{\\driftdowntblelev}\n"
			       "\\newlength{\\driftdowntblcomment}\n"
			       "\\settowidth{\\driftdowntblsitename}{\\narrowfont{}WWWWWW}\n"
			       "\\settowidth{\\driftdowntbllat}{\\narrowfont{}N88 88.88}\n"
			       "\\settowidth{\\driftdowntbllon}{\\narrowfont{}W888 88.88}\n"
			       "\\settowidth{\\driftdowntblelev}{\\narrowfont{}8888ft}\n"
			       "\\setlength{\\driftdowntblcomment}{0.5\\linewidth}\n"
			       "\\addtolength{\\driftdowntblcomment}{-\\driftdowntblsitename}\n"
			       "\\addtolength{\\driftdowntblcomment}{-\\driftdowntbllat}\n"
			       "\\addtolength{\\driftdowntblcomment}{-\\driftdowntbllon}\n"
			       "\\addtolength{\\driftdowntblcomment}{-\\driftdowntblelev}\n"
			       "\\addtolength{\\driftdowntblcomment}{-9.0\\tabcolsep}\n"
			       "%\\addtolength{\\driftdowntblcomment}{-4.5\\arrayrulewidth}\n\n"
			       "\\noindent\\begin{longtable}{@{}p{\\driftdowntblsitename}p{\\driftdowntbllat}"
			       "p{\\driftdowntbllon}p{\\driftdowntblelev}p{\\driftdowntblcomment}"
			       "p{\\driftdowntblsitename}p{\\driftdowntbllat}p{\\driftdowntbllon}"
			       "p{\\driftdowntblelev}p{\\driftdowntblcomment}@{}}\n");
		if (cnt & 1) {
			os << " & ";
		} else {
			if (cnt)
				os << " \\\\" << std::endl;
			os << "  ";
		}
		if (si->is_name_alphanum())
			os << "\\hypertarget{lnkdriftdown" << si->get_name() << '}';
		os << "{\\narrowfont{}" << METARTAFChart::latex_string_meta(si->get_name())
		   << "} & {\\narrowfont{}" << si->get_coord().get_lat_str3()
		   << "} & {\\narrowfont{}" << si->get_coord().get_lon_str3()
		   << "} & \\multicolumn{1}{r}{\\narrowfont{}" << si->get_elev() << "ft} & {\\narrowfont{}"
		   << METARTAFChart::latex_string_meta(si->get_comment()) << '}';
		++cnt;
	}
	if (!cnt)
		return;
	if (cnt & 1)
		os << " & & & & ";
	os << "\n\\end{longtable}\n\n";
}
