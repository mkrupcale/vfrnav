//
// C++ Interface: sitename
//
// Description:
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef SITENAME_H
#define SITENAME_H

class SiteName {
public:
	static constexpr char sitename[] = "autorouter.aero";
	static constexpr char siteurlnoproto[] = "www.autorouter.aero";
	static constexpr char siteurl[] = "http://www.autorouter.aero";
	static constexpr char sitesecureurl[] = "https://www.autorouter.aero";
};

#endif /* SITENAME_H */
