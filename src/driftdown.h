//
// C++ Interface: driftdown
//
// Description: Drift Down Calculations
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2014, 2015, 2016, 2017, 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DRIFTDOWN_H
#define DRIFTDOWN_H

#include <glibmm.h>
#include <iostream>
#include <cairomm/cairomm.h>

#include "sysdeps.h"
#include "aircraft.h"
#include "baro.h"
#include "geom.h"
#include "dbobj.h"
#include "grib2.h"
#include "wind.h"
#include "metgraph.h"

class DriftDown {
public:
	typedef MeteoProfile::DriftDownProfilePoint DriftDownProfilePoint;
	typedef MeteoProfile::DriftDownProfile DriftDownProfile;
	typedef Wind<double>::gribwind_t gribwind_t;
	typedef std::pair<Wind<double>, float> wx_t;
	typedef std::pair<int32_t, int32_t> minmaxalt_t;

	class GridPt {
	public:
		GridPt(int32_t elev = std::numeric_limits<int32_t>::max(),
		       int32_t alt = std::numeric_limits<int32_t>::min()) : m_elev(elev), m_alt(alt) {}
		int32_t get_elev(void) const { return m_elev; }
		int32_t get_alt(void) const { return m_alt; }
		void set_elev(int32_t e) { m_elev = e; }
		void set_alt(int32_t a) { m_alt = a; }

	protected:
		int32_t m_elev;
		int32_t m_alt;
	};

	DriftDown(const Point& pt = Point::invalid, int32_t elev = 0);
	void load_topo(TopoDb30& db, double maxradius);
	void load_wind(const GRIB2& wxdb, gint64 efftime);
	void calculate(const Aircraft::ClimbDescent& cd, int32_t arrheight = 500, int32_t terrainheight = 500, bool exact = false);
	MultiPolygonHole get_contour(int32_t alt) const;
	GridPt& operator()(int lat, int lon);
	const GridPt& operator()(int lat, int lon) const;
	GridPt& operator()(const TopoDb30::TopoCoordinate& tc);
	const GridPt& operator()(const TopoDb30::TopoCoordinate& tc) const;
	Rect get_rect(void) const;
	Point get_coord(int lat, int lon) const;
	Point get_coord(const TopoDb30::TopoCoordinate& tc) const;
	wx_t get_wx(int32_t alt) const; // wind in mps, temp in kelvin
	int32_t compute_startalt(const Aircraft::ClimbDescent& cd, const Point& p0, const Point& p1, int32_t alt1) const;
	minmaxalt_t get_minmaxalt(void) const;
	bool update_profile(DriftDownProfile& prof, const std::string& sitename);

protected:
	class QueuePt {
	public:
		QueuePt(int lat = 0, int lon = 0, int32_t alt = 0);
		int32_t get_lat(void) const { return m_lat; }
		int32_t get_lon(void) const { return m_lon; }
		int32_t get_alt(void) const { return m_alt; }
		int compare(const QueuePt& x) const;
		bool operator==(const QueuePt& x) const { return compare(x) == 0; }
		bool operator!=(const QueuePt& x) const { return compare(x) != 0; }
		bool operator<(const QueuePt& x) const { return compare(x) < 0; }
		bool operator<=(const QueuePt& x) const { return compare(x) <= 0; }
		bool operator>(const QueuePt& x) const { return compare(x) > 0; }
		bool operator>=(const QueuePt& x) const { return compare(x) >= 0; }

	protected:
		int32_t m_lat;
		int32_t m_lon;
		int32_t m_alt;
	};

	class Neighbor {
	public:
		typedef Aircraft::ClimbDescent::poly_t poly_t;

		Neighbor(int latdisp = 0, int londisp = 0);
		int get_latdisp(void) const { return m_latdisp; }
		int get_londisp(void) const { return m_londisp; }
		const poly_t& get_altpoly(void) const { return m_altpoly; }
		int32_t get_maxalt(void) const { return m_maxalt; }
		int32_t get_alt(int32_t alt1) const;
		void set_altpoly(const poly_t& ap, int32_t ma);

	protected:
		int m_latdisp;
		int m_londisp;
		poly_t m_altpoly;
		int32_t m_maxalt;
	};

	static const unsigned int nrweather = sizeof(GRIB2::WeatherProfilePoint::isobaric_levels) / sizeof(GRIB2::WeatherProfilePoint::isobaric_levels[0]);
	static const GridPt dummydata;
	typedef std::set<QueuePt> queue_t;
	typedef std::vector<GridPt> data_t;
	data_t m_data;
	gribwind_t m_wind[nrweather]; // knots, knots
	float m_temp[nrweather];  // kelvin
	TopoDb30::TopoCoordinate m_topoorigin;
	GridPt m_dummydata;
	gint64 m_minefftime;
	gint64 m_maxefftime;
	gint64 m_minreftime;
	gint64 m_maxreftime;
	Point m_coord;
	int32_t m_elev;
	int m_latsize;
	int m_lonsize;
	float m_qnh;

	Neighbor *calculate(Neighbor *neigh, const Aircraft::ClimbDescent& cd) const;
	void calculate(queue_t& queue, const Neighbor *neigh, const Neighbor *neighend, int latc, int lonc, int32_t alt1, int32_t terrainheight);
	void calculate(queue_t& queue, const Aircraft::ClimbDescent& cd, int latc, int lonc, const Point& p1, int32_t alt1, int32_t terrainheight);

	// contour extraction
	class Edge {
	public:
		Edge(const Point& st = Point::invalid, unsigned int ste = 0, const Point& en = Point::invalid, unsigned int ene = 0,
		     const Point& mid = Point::invalid) : m_start(st), m_end(en), m_intermediate(mid), m_startedge(ste), m_endedge(ene) {}
		const Point& get_start(void) const { return m_start; }
		const Point& get_end(void) const { return m_end; }
		const Point& get_intermediate(void) const { return m_intermediate; }
		unsigned int get_startedge(void) const { return m_startedge; }
		unsigned int get_endedge(void) const { return m_endedge; }
		int compare(const Edge& x) const;
		bool operator==(const Edge& x) const { return compare(x) == 0; }
		bool operator!=(const Edge& x) const { return compare(x) != 0; }
		bool operator<(const Edge& x) const { return compare(x) < 0; }
		bool operator<=(const Edge& x) const { return compare(x) <= 0; }
		bool operator>(const Edge& x) const { return compare(x) > 0; }
		bool operator>=(const Edge& x) const { return compare(x) >= 0; }

	protected:
		Point m_start;
		Point m_end;
		Point m_intermediate;
		unsigned int m_startedge;
		unsigned int m_endedge;
	};

	typedef std::set<Edge> edges_t;

	static Point interpolate(const Point& p0, const Point& p1, double frac);
	static double interpolate_factor(int32_t alt0, int32_t alt1, int32_t alt);
	static Point interpolate(const Point& p0, int32_t alt0, const Point& p1, int32_t alt1, int32_t alt);
	unsigned int get_index(unsigned int x, unsigned int y) const;
	unsigned int get_edgenr(unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1) const;
	void calculate(edges_t& edges, int32_t alt) const;
	static PolygonSimple extract_contour(edges_t& edges);
};

class DriftDownChart {
public:
	typedef DriftDown::DriftDownProfilePoint DriftDownProfilePoint;
	typedef DriftDown::DriftDownProfile DriftDownProfile;

	class Site {
	public:
		Site(const std::string& name = "", const Point& coord = Point::invalid,
		     int32_t elev = std::numeric_limits<int32_t>::min(), const std::string& comment = "");
		Site(const DbBaseElements::Airport& arpt);
		const std::string& get_name(void) const { return m_name; }
		void set_name(const std::string& n) { m_name = n; }
		const std::string& get_comment(void) const { return m_comment; }
		void set_comment(const std::string& c) { m_comment = c; }
		const Point& get_coord(void) const { return m_coord; }
		void set_coord(const Point& pt) { m_coord = pt; }
		int32_t get_elev(void) const { return m_elev; }
		void set_elev(int32_t el) { m_elev = el; }
		void set(const DbBaseElements::Airport& arpt);
		bool parse(const std::string& x);
		bool is_name_alphanum(void) const;
		int compare(const Site& x) const { return get_name().compare(x.get_name()); }
		bool operator==(const Site& x) const { return compare(x) == 0; }
		bool operator!=(const Site& x) const { return compare(x) != 0; }
		bool operator<(const Site& x) const { return compare(x) < 0; }
		bool operator<=(const Site& x) const { return compare(x) <= 0; }
		bool operator>(const Site& x) const { return compare(x) > 0; }
		bool operator>=(const Site& x) const { return compare(x) >= 0; }

	protected:
		std::string m_name;
		std::string m_comment;
		Point m_coord;
		int32_t m_elev;
	};

	typedef std::vector<Site> sites_t;

	class Contour {
	public:
		Contour(int32_t alt = std::numeric_limits<int32_t>::min(),
			double width = 2, uint8_t red = 0, uint8_t green = 0, uint8_t blue = 0,
			const std::string& tikzstroke = "",
			uint8_t redfill = 255, uint8_t greenfill = 255, uint8_t bluefill = 255,
			const std::string& tikzfill = "");
		int32_t get_alt(void) const { return m_alt; }
		bool operator==(const Contour& x) const { return get_alt() == x.get_alt(); }
		bool operator!=(const Contour& x) const { return get_alt() != x.get_alt(); }
		bool operator<(const Contour& x) const { return get_alt() < x.get_alt(); }
		bool operator<=(const Contour& x) const { return get_alt() <= x.get_alt(); }
		bool operator>(const Contour& x) const { return get_alt() > x.get_alt(); }
		bool operator>=(const Contour& x) const { return get_alt() >= x.get_alt(); }
		double get_width(void) const { return m_width; }
		void get_stroke(uint8_t& red, uint8_t& green, uint8_t& blue) const;
		void get_stroke(double& red, double& green, double& blue) const;
		void get_fill(uint8_t& red, uint8_t& green, uint8_t& blue) const;
		void get_fill(double& red, double& green, double& blue) const;
		const MultiPolygonHole& get_poly(void) const { return m_poly; }
		void set_poly(const MultiPolygonHole& p) { m_poly = p; }
		void add_poly(const MultiPolygonHole& p);
		const std::string& get_tikzstroke(void) const { return m_tikzstroke; }
		const std::string& get_tikzfill(void) const { return m_tikzfill; }
		void set_stroke(const Cairo::RefPtr<Cairo::Context>& cr) const;
		void set_fill(const Cairo::RefPtr<Cairo::Context>& cr) const;
		std::string latex_strokecol(void) const;
		std::string latex_fillcol(void) const;
		std::string define_latex_strokecol(void) const;
		std::string define_latex_fillcol(void) const;
		bool is_stroke(void) const { return m_width > 0; }
		bool is_fill(void) const { return m_rgbfill[0] != 255 || m_rgbfill[1] != 255 || m_rgbfill[1] != 255; }

	protected:
		MultiPolygonHole m_poly;
		std::string m_tikzstroke;
		std::string m_tikzfill;
		double m_width;
		int32_t m_alt;
		uint8_t m_rgbstroke[3];
		uint8_t m_rgbfill[3];
	};

	typedef std::set<Contour> contours_t;

	typedef std::vector<FPlanAlternate> alternates_t;
	typedef std::vector<FPlanRoute> altroutes_t;

	DriftDownChart(const FPlanRoute& route = FPlanRoute(*(FPlan *)0),
		       const alternates_t& altn = alternates_t(),
		       const altroutes_t& altnfpl = altroutes_t());

	const FPlanRoute& get_route(void) const { return m_route; }
	void set_route(const FPlanRoute& r = FPlanRoute(*(FPlan *)0)) { m_route = r; }
	const alternates_t& get_alternates(void) const { return m_altn; }
	void set_alternates(const alternates_t& a = alternates_t()) { m_altn = a; }
	const altroutes_t& get_altnroutes(void) const { return m_altnroutes; }
	void set_altnroutes(const altroutes_t& r) { m_altnroutes = r; }
	void add_fir(const std::string& id = "", const MultiPolygonHole& poly = MultiPolygonHole());
	Rect get_bbox(void) const;
	Rect get_bbox(unsigned int wptidx0, unsigned int wptidx1) const;
	Rect get_extended_bbox(void) const;
	Rect get_extended_bbox(unsigned int wptidx0, unsigned int wptidx1) const;
	std::pair<time_t,time_t> get_timebound(void) const;

	void draw(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
	void draw(std::ostream& os, std::vector<std::string>& tempfiles, const std::string& tempdir, double width, double height);
	void draw(std::ostream& os, double width, double height);
	void draw_legend(std::ostream& os);
	void define_latex_colors(std::ostream& os);
	void draw_sitelist(std::ostream& os);

	virtual void draw_bgndlayer(const Cairo::RefPtr<Cairo::Context>& cr, const Rect& bbox,
				    const Point& center, double scalelon, double scalelat) {}
	virtual void draw_bgndlayer(std::ostream& os, const Rect& bbox, const Point& center,
				    double width, double height, double scalelon, double scalelat) {}

	sites_t& get_sites(void) { return m_sites; }
	const sites_t& get_sites(void) const { return m_sites; }
	void set_sites(const sites_t& s) { m_sites = s; }
	void add_site(const Site& s) { m_sites.push_back(s); }
	contours_t& get_contours(void) { return m_contours; }
	const contours_t& get_contours(void) const { return m_contours; }
	void set_contours(const contours_t& c) { m_contours = c; }
	bool add_contour(const Contour& c) { return m_contours.insert(c).second; }
	const DriftDownProfile& get_profile(void) const { return m_profile; }

	const FPlanWaypoint *find_nearest_fplanwpt(const Point& pt) const;
	void calculate(TopoDb30& topodb, const GRIB2& wxdb, gint64 efftime, const Aircraft& acft,
		       int32_t arrheight = 500, int32_t terrainheight = 500);
	static DriftDownProfile calculate_profile(const FPlanRoute& route);

protected:
        static constexpr double max_dist_from_route = 50.;
	static constexpr double chart_strip_length = 500.;

	static constexpr double tex_char_width = 0.25 * 24.01172 * 0.035277778;
	static constexpr double tex_char_height = 6.50241 * 0.035277778;

	FPlanRoute m_route;
	alternates_t m_altn;
	altroutes_t m_altnroutes;
	sites_t m_sites;
	contours_t m_contours;
	MetarTafSet m_set;
	DriftDownProfile m_profile;

	static inline void set_color_fplan(const Cairo::RefPtr<Cairo::Context>& cr) {
		cr->set_source_rgb(1., 0., 1.);
	}

	static inline void set_color_ctrybdry(const Cairo::RefPtr<Cairo::Context>& cr) {
		cr->set_source_rgb(0., 0., 0.);
	}

	void draw_firs(std::ostream& os, METARTAFChart::LabelLayout& lbl, const Rect& bbox, const Point& center,
		       double width, double height, double scalelon, double scalelat);
	void draw(unsigned int wptidx0, unsigned int wptidx1, const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
	void draw(unsigned int wptidx0, unsigned int wptidx1, std::ostream& os, double width, double height);
	unsigned int get_chart_strip(unsigned int wptstart) const;
};

#endif /* DRIFTDOWN_H */
