/*
 * grib2fcst.cc:  Extract GRIB2 Forecast for Winds Aloft
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

#include "sysdeps.h"

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <limits>
#include <sqlite3x.hpp>
#include <sqlite3.h>
#include <unistd.h>
#include <stdexcept>
#include <glibmm.h>
#include <giomm.h>

#include "geom.h"
#include "dbobj.h"
#include "engine.h"
#include "icaofpl.h"
#include "baro.h"
#include "metgraph.h"

/* ---------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
        static struct option long_options[] = {
		{ "maindir", required_argument, 0, 'm' },
		{ "auxdir", required_argument, 0, 'a' },
		{ "disableaux", no_argument, 0, 'A' },
		{ "time", required_argument, 0, 't' },
		{ "date", required_argument, 0, 't' },
		{ "level", required_argument, 0, 'F' },
		{ "flightlevel", required_argument, 0, 'F' },
		{ "fl", required_argument, 0, 'F' },
		{ "gramet", no_argument, 0, 'g' },
		{ "profile", no_argument, 0, 'p' },
		{ "gfs", no_argument, 0, 0x400 + Engine::grib2source_gfs },
		{ "iconglobal", no_argument, 0, 0x400 + Engine::grib2source_iconglobal },
		{ "iconeu", no_argument, 0, 0x400 + Engine::grib2source_iconeu },
		{ "nowindtransform", no_argument, 0, 0x500 },
		{ "windtransform", no_argument, 0, 0x501 },
		{ "polarwind", no_argument, 0, 0x502 },
		{ "cartesianwind", no_argument, 0, 0x503 },
		{0, 0, 0, 0}
        };
	Glib::init();
	Gio::init();
        Glib::ustring dir_main(""), dir_aux("");
	Engine::auxdb_mode_t auxdbmode(Engine::auxdb_prefs);
	Engine::grib2source_t source(Engine::grib2source_gfs);
	bool dis_aux(false), windtransform(true), cartesianwind(false), gramet(false), profile(false);
	time_t tm(time(0));
	unsigned int baselevel(100), toplevel(100);
        int c, err(0);

        while ((c = getopt_long(argc, argv, "m:a:At:F:gp", long_options, 0)) != EOF) {
                switch (c) {
		case 'm':
			if (optarg)
				dir_main = optarg;
			break;

		case 'a':
			if (optarg)
				dir_aux = optarg;
			break;

		case 'A':
			dis_aux = true;
			break;

		case 't':
			if (optarg) {
				Glib::TimeVal tv;
				if (tv.assign_from_iso8601(optarg)) {
					tm = tv.tv_sec;
				} else {
					std::cerr << "grib2fcst: invalid time " << optarg << std::endl;
					++err;
				}
			}
			break;

		case 'F':
			if (optarg) {
				char *cp;
				baselevel = strtoul(optarg, &cp, 0);
				if (cp && *cp == ',')
					toplevel = strtoul(cp + 1, 0, 0);
				else
					toplevel = baselevel;
				if (baselevel > toplevel)
					std::swap(baselevel, toplevel);
			}
			break;

		case 'g':
			gramet = true;
			break;

		case 'p':
			profile = true;
			break;

		case 0x400 + Engine::grib2source_gfs:
		case 0x400 + Engine::grib2source_iconglobal:
		case 0x400 + Engine::grib2source_iconeu:
			source = (Engine::grib2source_t)(c - 0x400);
			break;

		case 0x500:
		case 0x501:
			windtransform = c & 1;
			break;

		case 0x502:
		case 0x503:
			cartesianwind = c & 1;
			break;

		default:
			++err;
			break;
                }
        }
        if (err) {
                std::cerr << "usage: grib2fcst [-m <maindir>] [-a <auxdir>] [-A] [-t <time>] [-F <level>[,<level>]] [--nowindtransform] [--cartesianwind]" << std::endl;
                return EX_USAGE;
        }
	if (dis_aux)
		auxdbmode = Engine::auxdb_none;
	else if (!dir_aux.empty())
		auxdbmode = Engine::auxdb_override;
        try {
		Engine m_engine(dir_main, auxdbmode, dir_aux, false, false);
		TopoDb30 m_topodb;
		if (gramet)
			m_topodb.open_readonly(dir_aux.empty() ? Engine::get_default_aux_dir() : (std::string)dir_aux, true);
		// load GRIB2
		{
			GRIB2::Parser p(m_engine.get_grib2_db(source));
			std::string path1(dir_main);
                        if (path1.empty())
                                path1 = FPlan::get_userdbpath();
                        path1 = Glib::build_filename(path1, Engine::grib2_default_dir(source));
			p.parse(path1);
			unsigned int count1(m_engine.get_grib2_db(source).find_layers().size()), count2(0);
                        std::string path2(m_engine.get_aux_dir(auxdbmode, dir_aux));
			if (!path2.empty()) {
				path2 = Glib::build_filename(path2, Engine::grib2_default_dir(source));
				p.parse(path2);
				count2 = m_engine.get_grib2_db(source).find_layers().size() - count1;
			}
			std::cout << "Loaded " << count1 << " GRIB2 Layers from " << path1;
			if (!path2.empty())
				std::cout << " and " << count2 << " GRIB2 Layers from " << path2;
			std::cout << std::endl;
			unsigned int count3(m_engine.get_grib2_db(source).expire_cache());
			if (count3)
				std::cout << "Expired " << count3 << " cached GRIB2 Layers" << std::endl;
		}
		Point coord;
		coord.set_invalid();
		for (int i = optind; i < argc; ++i) {
			Glib::ustring coordtxt(argv[i]);
			FPlanRoute route(*(FPlan *)0);
			{
				IcaoFlightPlan::FindCoord fc(m_engine);
				if (!fc.find(coordtxt, "", IcaoFlightPlan::FindCoord::flag_airport |
					     IcaoFlightPlan::FindCoord::flag_navaid, coord) &&
				    !fc.find("", coordtxt, IcaoFlightPlan::FindCoord::flag_waypoint |
					     IcaoFlightPlan::FindCoord::flag_coord, coord)) {
					std::cerr << "Point " << coordtxt << " not found" << std::endl;
					continue;
				}
				coord = fc.get_coord();
				coordtxt = fc.get_icao();
				if (!fc.get_name().empty()) {
					if (!coordtxt.empty())
						coordtxt += " ";
					coordtxt += fc.get_name();
				}
				{
					FPlanWaypoint wpt;
					wpt.set_icao(fc.get_icao());
					wpt.set_name(fc.get_name());
					wpt.set_coord(fc.get_coord());
					wpt.set_time_unix(tm - 2*3600);
					wpt.set_flighttime(0);
					route.insert_wpt(~0U, wpt);
					wpt.set_flighttime(4*3600);
					route.insert_wpt(~0U, wpt);
				}
			}
			Rect bbox(Rect(coord, coord).oversize_nmi(100.f));
			for (unsigned int level = baselevel; level <= toplevel; level += 10) {
				float press(0);
				IcaoAtmosphere<float>::std_altitude_to_pressure(&press, 0, level * (100 * Point::ft_to_m));
				press *= 100;
				boost::intrusive_ptr<GRIB2::LayerInterpolateResult> windu, windv, prmsl, temperature, cldcover, icingdeg, eddydissr;
				{
					GRIB2::layerlist_t ll(m_engine.get_grib2_db(source).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_momentum_ugrd),
													tm, GRIB2::Layer::statproc_t::none, GRIB2::surface_isobaric_surface, press));
					windu = GRIB2::interpolate_results(bbox, ll, tm, press);
				}
				{
					GRIB2::layerlist_t ll(m_engine.get_grib2_db(source).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_momentum_vgrd),
													tm, GRIB2::Layer::statproc_t::none, GRIB2::surface_isobaric_surface, press));
					windv = GRIB2::interpolate_results(bbox, ll, tm, press);
				}
				{
					GRIB2::layerlist_t ll(m_engine.get_grib2_db(source).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_temperature_tmp),
													tm, GRIB2::Layer::statproc_t::none, GRIB2::surface_isobaric_surface, press));
					temperature = GRIB2::interpolate_results(bbox, ll, tm, press);
				}
				{
					GRIB2::layerlist_t ll(m_engine.get_grib2_db(source).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_mass_prmsl),
													tm, GRIB2::Layer::statproc_t::none));
					prmsl = GRIB2::interpolate_results(bbox, ll, tm);
				}
				{
					GRIB2::layerlist_t ll(m_engine.get_grib2_db(source).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_cloud_cdcc),
													tm, GRIB2::Layer::statproc_t::none, GRIB2::surface_isobaric_surface, press));
					cldcover = GRIB2::interpolate_results(bbox, ll, tm);
				}
				{
					GRIB2::layerlist_t ll(m_engine.get_grib2_db(source).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_icingdegree),
													tm, GRIB2::Layer::statproc_t::none, GRIB2::surface_isobaric_surface, press));
					icingdeg = GRIB2::interpolate_results(bbox, ll, tm);
				}
				{
					GRIB2::layerlist_t ll(m_engine.get_grib2_db(source).find_layers(GRIB2::find_parameter(GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_eddydissrate),
													tm, GRIB2::Layer::statproc_t::none, GRIB2::surface_isobaric_surface, press));
					eddydissr = GRIB2::interpolate_results(bbox, ll, tm);
				}
				if (!coordtxt.empty())
					std::cout << coordtxt << ' ';
				std::cout << coord.get_lat_str2() << ' ' << coord.get_lon_str2() << " F" << std::setw(3) << std::setfill('0') << level
					  << ' ' << Glib::TimeVal(tm, 0).as_iso8601() << std::fixed;
				if (windu && windv) {
					float wu(windu->operator()(coord, tm, press));
					float wv(windv->operator()(coord, tm, press));
					std::pair<float,float> wtransf(wu, wv);
					if (windtransform)
						wtransf = windu->get_layer()->get_grid()->transform_axes(wu, wv);
					// convert from m/s to kts
					std::cout << " WIND ";
					if (cartesianwind) {
						std::ostringstream oss;
						oss << std::fixed << std::setprecision(1) << (wtransf.first * (-1e-3f * Point::km_to_nmi * 3600))
						    << ", " << (wtransf.second * (-1e-3f * Point::km_to_nmi * 3600));
						std::cout << oss.str();
					} else {
						std::pair<float,float> r(GRIB2::Grid::axes_to_dirmag_kts(wtransf));
						std::cout << std::setw(3) << std::setfill('0')
							  << Point::round<int,float>(r.first)
							  << '/' << std::setw(2) << std::setfill('0')
							  << Point::round<int,float>(r.second);
					}
				}
				if (temperature) {
					std::cout << " OAT " << std::setprecision(1)
						  << (temperature->operator()(coord, tm, press) - IcaoAtmosphere<double>::degc_to_kelvin);
				}
				if (prmsl) {
					std::cout << " QFF " << std::setprecision(1) << (prmsl->operator()(coord, tm, press) * 0.01f);
				}
				if (cldcover) {
					std::cout << " CLDCOV " << std::setprecision(1) << (cldcover->operator()(coord, tm, press) * 0.01f);
				}
				if (icingdeg) {
					std::cout << " ICINGDEG " << std::setprecision(1) << icingdeg->operator()(coord, tm, press);
				}
				if (eddydissr) {
					std::cout << " EDDY " << std::setprecision(1) << eddydissr->operator()(coord, tm, press);
				}
				std::cout << std::endl;
			}
			if (gramet || profile) {
				MeteoProfile prof(route);
				prof.set_mtom(1000);
				try {
					prof.set_wxprofile(m_engine.get_grib2_db(source).get_profile(route));
				} catch (const std::exception& e) {
					std::cerr << "wx profile error: " << e.what() << std::endl;
					continue;
				}
				if (profile)
					prof.get_wxprofile().print(std::cout);
				if (!gramet)
					continue;
				try {
					prof.set_routeprofile(m_topodb.get_profile(route, 5));
				} catch (const std::exception& e) {
					std::cerr << "topo db error: " << e.what() << std::endl;
					continue;
				}
				{
					std::string err(prof.check_error());
					if (!err.empty()) {
						std::cerr << err << std::endl;
						continue;
					}
				}
				char fname[] = "/tmp/gramet-XXXXXX.pdf";
				if (mkstemps(fname, 4) == -1) {
					std::cerr << "Cannot create temp file" << std::endl;
					continue;
				}
				static constexpr double height(595.28), width(841.89);
				static constexpr MeteoProfile::yaxis_t profileyaxis(MeteoProfile::yaxis_pressure);
				double profilemaxalt(route.max_altitude() + 4000);
				if (profileyaxis == MeteoProfile::yaxis_pressure)
					profilemaxalt = 60000;
				Cairo::RefPtr<Cairo::Surface> surface;
				Cairo::RefPtr<Cairo::PdfSurface> pdfsurface;
				Cairo::RefPtr<Cairo::Context> ctx;
				surface = pdfsurface = Cairo::PdfSurface::create(fname, width, height);
				ctx = Cairo::Context::create(surface);
				ctx->translate(20, 20);
				gint64 duration(0);
				if (!prof.get_wxprofile().empty())
					duration = prof.get_wxprofile().back().get_efftime() - prof.get_wxprofile().front().get_efftime();
				double scaledist(prof.get_scaledist(ctx, width - 40, duration));
				double scaleelev, originelev;
				if (profileyaxis == MeteoProfile::yaxis_pressure) {
					scaleelev = prof.get_scaleelev(ctx, height - 40, profilemaxalt, profileyaxis);
					originelev = IcaoAtmosphere<double>::std_sealevel_pressure;
				} else {
					scaleelev = prof.get_scaleelev(ctx, height - 40, profilemaxalt, profileyaxis);
					originelev = 0;
				}
				prof.draw(ctx, width - 40, height - 40, 0, scaledist, originelev, scaleelev, profileyaxis);
				ctx->show_page();
				//surface->show_page();
				std::cout << "GRAMET " << fname << std::endl;
			}
		}
        } catch (const Glib::Exception& ex) {
                std::cerr << "Glib exception: " << ex.what() << std::endl;
                return EX_DATAERR;
        } catch (const std::exception& ex) {
                std::cerr << "exception: " << ex.what() << std::endl;
                return EX_DATAERR;
        }
        return EX_OK;
}
